package org.example;



import org.example.entity.Entity;
import org.junit.jupiter.api.Test;

import java.awt.event.KeyEvent;



class KeyboardTest {

    Entity entity;
    GamePanel gp;


    @Test
    void titleState() {  //testing menu(title list), if keyboard and ">" point works right and if after the enter button we can play (have playState)

        Keyboard keyboard = new Keyboard(new GamePanel());

        keyboard.titleState(KeyEvent.VK_DOWN);
        // Assert the expected value after pressing VK_DOWN
        assert keyboard.gp.ui.commandNum == 1;
        keyboard.titleState(KeyEvent.VK_DOWN);
        // Assert the expected value after pressing VK_DOWN
        assert keyboard.gp.ui.commandNum == 2;
        keyboard.titleState(KeyEvent.VK_DOWN);
        // Assert the expected value after pressing VK_DOWN
        assert keyboard.gp.ui.commandNum == 0;

        keyboard.titleState(KeyEvent.VK_W);
        // Assert the expected value after pressing VK_W
        assert keyboard.gp.ui.commandNum == 2;
        keyboard.titleState(KeyEvent.VK_DOWN);
        // Assert the expected value after pressing VK_DOWN
        assert keyboard.gp.ui.commandNum == 0;

        keyboard.titleState(KeyEvent.VK_ENTER);
        assert keyboard.gp.gameState == keyboard.gp.playState;
    }

    @Test
    public void testPauseState() { //testing if pause state in game works (we see PAUSE screen) and otherwise

        Keyboard keyboard = new Keyboard(new GamePanel());

        keyboard.playState(KeyEvent.VK_Q);
        // Assert the expected pause state after pressing VK_Q
        assert keyboard.gp.gameState == keyboard.gp.pauseState;

        keyboard.pauseState(KeyEvent.VK_Q);
        // Assert the expected play state again after pressing VK_Q
        assert keyboard.gp.gameState == keyboard.gp.playState;
    }


    @Test
    public void testGameOverState(){
        Keyboard keyboard = new Keyboard(new GamePanel());
        gp = new GamePanel();
        entity = new Entity(gp);

        gp.player.life = 0;
        gp.player.update();
        assert gp.gameState == gp.gameOverState;

    }
}