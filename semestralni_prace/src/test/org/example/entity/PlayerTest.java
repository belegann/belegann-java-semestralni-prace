package org.example.entity;

import org.example.EventHandler;
import org.example.GamePanel;
import org.example.Keyboard;
import org.example.monster.MON_GoldAmogus;
import org.example.object.OBJ_Potion_Gold;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
//Integrální test pro přechod hráče na druhou úroveň po spuštění hry
class PlayerTest {

    GamePanel gp;
    EventHandler eventH;
    Keyboard keyboard;
    Entity entity;
    OBJ_Potion_Gold potion;
    MON_GoldAmogus monster;

    @BeforeEach
    public void setUp() {
        gp = new GamePanel();
        eventH = new EventHandler(gp);
        keyboard = new Keyboard(gp);
        entity = new Entity(gp);
        potion = new OBJ_Potion_Gold(gp);
        monster = new MON_GoldAmogus(gp);
    }

    @Test
    void setDefaultValues() {

        int x = 4;
        int y = 4;

        assertEquals(gp.tileSize*x, gp.player.worldX);
        assertEquals(gp.tileSize*y, gp.player.worldY);


        assertEquals(6, gp.player.life);
        assertEquals(6, gp.player.maxLife);

        assertEquals(1, gp.player.level);
        assertEquals(0, gp.player.exp);
        assertEquals(3, gp.player.nextLevelExp);

    }

    @Test
    void addingPotionInInventoryWorks(){
        //gp.player.worldX = gp.tileSize * 5;
        //gp.player.worldY = gp.tileSize * 4;

        assertTrue(gp.player.canObtainItem(potion));

        gp.obj[2] = potion;
        gp.player.pickUpObject(2);
        assertNull(gp.obj[2]);
        assertEquals(3, gp.player.inventory.size());
    }



    //pouzivam potion

}