package org.example;

import org.example.entity.Entity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EventHandlerTest {

    GamePanel gp;
    EventHandler eventH;
    Keyboard keyboard;
    Entity entity;

    @BeforeEach
    public void setUp() {
        gp = new GamePanel();
        eventH = new EventHandler(gp);
        keyboard = new Keyboard(gp);
        entity = new Entity(gp);
    }

    @Test
    public void testLavaEvent() { // testing if lava invent is working

        // Parametrs of 1 LavaEvent tile
        int col = 32;
        int row = 1;

        gp.player.life = 10; //life of player
        keyboard.gp.gameState = 3; //gameState = 3, means dialogueStatue


        eventH.Lava(col, row, keyboard.gp.dialogueState); //Lava ivent happens


        assertEquals(9, gp.player.life); //after this event player's life should be lower by 1 (-1)
        assertFalse(eventH.canTouchEvent); //Should be false because canTouchEvent is true than
        // player is more than 1 tile away from the last event

        assertEquals(keyboard.gp.dialogueState, keyboard.gp.gameState); //checking if we have dialogue statue

        assertEquals("You are in LAVA!!!", gp.ui.currentDialogue); //checking if we have message
    }

    @Test
    public void testMilkEvent(){ // testing if milk invent is working, it update player's health and mana to max

        gp.player.life = 1;
        gp.player.mana = 1;

        eventH.Milk(6, 6, keyboard.gp.dialogueState);


        assertEquals(6, gp.player.life);
        assertEquals(4, gp.player.mana);
        assertEquals("You are in MILK!\n"+
                "The progress has been saved.", gp.ui.currentDialogue);

    }
}