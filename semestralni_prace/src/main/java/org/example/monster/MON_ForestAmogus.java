package org.example.monster;

import org.example.entity.Entity;

import org.example.GamePanel;
import org.example.object.OBJ_Coin;
import org.example.object.OBJ_Mana;
import org.example.object.OBJ_Rock;

import java.util.Random;

public class MON_ForestAmogus extends Entity {

    GamePanel gp;
    public MON_ForestAmogus(GamePanel gp) {
        super(gp);

        this.gp = gp;

        type = type_monster;
        name = "Forest Amogus";
        speed = 5;
        maxLife = 4;
        life = maxLife;

        projectile = new OBJ_Rock(gp);

        //INFLUENCE ON CHARACTER STATUS
        attack = 4;
        defense = 0;
        exp = 1;

        //solidArea.x = 48;
        //solidArea.y = 48;
        solidArea.width = 48;
        solidArea.height = 48;
        solidAreaDefaultX = solidArea.x;
        solidAreaDefaultY = solidArea.y;

        getImage();
    }

    public void getImage() { //loading and scaling images

        up1 = setup("res/monster/forestamodusenemy_up_1", gp.tileSize, gp.tileSize);
        up2 = setup("res/monster/forestamodusenemy_up_1", gp.tileSize, gp.tileSize);
        down1 = setup("res/monster/forestamodusenemy_down_1", gp.tileSize, gp.tileSize);
        down2 = setup("res/monster/forestamodusenemy_down_2", gp.tileSize, gp.tileSize);
        left1 = setup("res/monster/forestamodusenemy_left_1", gp.tileSize, gp.tileSize);
        left2 = setup("res/monster/forestamodusenemy_left_1", gp.tileSize, gp.tileSize);
        right1 = setup("res/monster/forestamodusenemy_right_1", gp.tileSize, gp.tileSize);
        right2 = setup("res/monster/forestamodusenemy_right_1", gp.tileSize, gp.tileSize);

    }

    public void setAction() { //setting amogus behavior

        actionLockCounter++;

        if(actionLockCounter == 120) {
            Random random = new Random();
            int i = random.nextInt(100)+1; //from 0 to 100 - random

            if(i <= 25 ) {
                direction = "up";
            }

            if(i > 25  && i <= 50) {
                direction = "down";
            }

            if(i > 50 && i <= 75 ) {
                direction = "left";
            }

            if(i > 75 && i <= 100) {
                direction = "right";
            }
            actionLockCounter = 0;

        }
        int i = new Random().nextInt(100) + 1;
        if(i > 99 && !projectile.alive && shotAvailableCounter == 30){
            projectile.set(worldX, worldY, direction, true, this);
            gp.projectileList.add(projectile);
            shotAvailableCounter = 0;
        }
    }

    public void damageReaction(){ //moving away from the player when receiving damage
        actionLockCounter = 0;
        direction = gp.player.direction;
    }
    public void checkDrop(){

        //cast a die
        int i = new Random().nextInt(100)+1;

        //set the monster drop
        if(i < 50){
            dropItem(new OBJ_Coin(gp));
        }
        if(i >= 50 && i < 100){
            dropItem(new OBJ_Mana(gp));
        }
    }
}
