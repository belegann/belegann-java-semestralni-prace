package org.example;

import org.example.entity.Entity;

public class CollisionChecker {

    GamePanel gp;
    public CollisionChecker(GamePanel gp) {
        this.gp = gp;
    }

    /**
     * Checks for collision between an entity and the tiles.
     * Determines if the entity collides with any solid tiles based on its position and direction.
     *
     * @param entity The entity to check for collision.
     */
    public void checkTile(Entity entity) { //if player touches collision (solid tile)
        //we need to look at player collision area in tile
        int entityLeftWorldX = entity.worldX + entity.solidArea.x;
        int entityRightWorldX = entity.worldX + entity.solidArea.x + entity.solidArea.width;
        int entityTopWorldY = entity.worldY + entity.solidArea.y;
        int entityBottomWorldY = entity.worldY + entity.solidArea.y + entity.solidArea.height;

        int entityLeftCol = entityLeftWorldX/gp.tileSize;
        int entityRightCol = entityRightWorldX/gp.tileSize;
        int entityTopRow = entityTopWorldY/gp.tileSize;
        int entityBottomRow = entityBottomWorldY/gp.tileSize;

        int tileNum1, tileNum2;

        switch(entity.direction) {
            case "up":
                entityTopRow = (entityTopWorldY - entity.speed)/gp.tileSize;
                tileNum1 = gp.tileM.mapTileNum[entityLeftCol][entityTopRow];
                tileNum2 = gp.tileM.mapTileNum[entityRightCol][entityTopRow];
                if(gp.tileM.tile[tileNum1].collision || gp.tileM.tile[tileNum2].collision) {
                    entity.collisionOn  = true;
                }
                break;
            case "down":
                entityBottomRow = (entityBottomWorldY + entity.speed)/gp.tileSize;
                tileNum1 = gp.tileM.mapTileNum[entityLeftCol][entityBottomRow];
                tileNum2 = gp.tileM.mapTileNum[entityRightCol][entityBottomRow];
                if(gp.tileM.tile[tileNum1].collision || gp.tileM.tile[tileNum2].collision) {
                    entity.collisionOn  = true;
                }
                break;
            case "left":
                entityLeftCol = (entityLeftWorldX - entity.speed)/gp.tileSize;
                tileNum1 = gp.tileM.mapTileNum[entityLeftCol][entityTopRow];
                tileNum2 = gp.tileM.mapTileNum[entityLeftCol][entityBottomRow];
                if(gp.tileM.tile[tileNum1].collision || gp.tileM.tile[tileNum2].collision) {
                    entity.collisionOn  = true;
                }
                break;
            case "right":
                entityRightCol = (entityRightWorldX + entity.speed)/gp.tileSize;
                tileNum1 = gp.tileM.mapTileNum[entityRightCol][entityTopRow];
                tileNum2 = gp.tileM.mapTileNum[entityRightCol][entityBottomRow];
                if(gp.tileM.tile[tileNum1].collision || gp.tileM.tile[tileNum2].collision) {
                    entity.collisionOn  = true;
                }
                break;
        }

    }

    /**
     * Checks for collision between an entity and objects.
     * Determines if the entity collides with any objects based on its position and direction.
     *
     * @param entity The entity to check for collision.
     * @param player Indicates whether the entity is the player for interaction objects.
     * @return The index of the object if a collision with the player occurs, otherwise returns 999.
     */
    public int checkObject(Entity entity, boolean player) { //checking IF this entity is PLAYER for interaction objects,
        //and checking if the player is hitting any objects
        int index = 999;
        for (int i=0; i < gp.obj.length; i++) { //scanning objects array
            if(gp.obj[i] != null) {
                //get entity's solid area position
                entity.solidArea.x = entity.worldX + entity.solidArea.x;
                entity.solidArea.y = entity.worldY + entity.solidArea.y;
                //get the object's solid area position
                gp.obj[i].solidArea.x = gp.obj[i].worldX + gp.obj[i].solidArea.x;
                gp.obj[i].solidArea.y = gp.obj[i].worldY + gp.obj[i].solidArea.y;
                /* since we have set 0 to the object's solid area's x and y the latter part doesn't
                add anything. but we included that so the code still works even if you set specific values
                in each object */

                switch (entity.direction) {
                    //simulating entity's movement and check where it will be after it moved
                    case "up":
                        entity.solidArea.y -= entity.speed; break;
                    case "down":
                        entity.solidArea.y += entity.speed; break;
                    case "left":
                        entity.solidArea.x -= entity.speed; break;
                    case "right":
                        entity.solidArea.x += entity.speed; break;
                }

                if(entity.solidArea.intersects(gp.obj[i].solidArea)) { //check if entity touches the OBJECT
                    if (gp.obj[i].collision == true) {
                        entity.collisionOn = true;
                    }
                    if (player == true) {
                        index = i;
                    }
                }
                entity.solidArea.x = entity.solidAreaDefaultX; //reset the solid area
                entity.solidArea.y = entity.solidAreaDefaultY; //reset the solid area
                gp.obj[i].solidArea.x = gp.obj[i].solidAreaDefaultX;
                gp.obj[i].solidArea.y = gp.obj[i].solidAreaDefaultY;
            }
        }
        return index; //returning index of the object
    }

    /**
     * Checks for collision between an entity and other entities.
     * Determines if the entity collides with any other entities based on its position and direction.
     *
     * @param entity The entity to check for collision.
     * @param target An array of entities to check collision against.
     * @return The index of the collided entity if a collision occurs, otherwise returns 999.
     */
    public int checkEntity(Entity entity, Entity[] target){ //npc or monster collision
        //if collision didn't happen, returns 999
        int index = 999;

        for (int i = 0; i < target.length; i++) { //scanning objects array
            if(target[i] != null) {

                //get entity's solid area position
                entity.solidArea.x = entity.worldX + entity.solidArea.x;
                entity.solidArea.y = entity.worldY + entity.solidArea.y;
                //get the object's solid area position
                target[i].solidArea.x = target[i].worldX + target[i].solidArea.x;
                target[i].solidArea.y = target[i].worldY + target[i].solidArea.y;
                /* since we have set 0 to the object's solid area's x and y the latter part doesn't
                add anything. but we included that so the code still works even if you set specific values
                in each object */

                switch (entity.direction) {
                    //simulating entity's movement and check where it will be after it moved
                    case "up":
                        entity.solidArea.y -= entity.speed; break;
                    case "down":
                        entity.solidArea.y += entity.speed; break;
                    case "left":
                        entity.solidArea.x -= entity.speed; break;
                    case "right":
                        entity.solidArea.x += entity.speed; break;
                }

                if(entity.solidArea.intersects(target[i].solidArea)) { //check if entity touches the OBJECT
                    //System.out.println("up collision!");
                    if(target[i] != entity){
                        entity.collisionOn = true;
                        index = i;
                    }
                }

                entity.solidArea.x = entity.solidAreaDefaultX; //reset the solid area
                entity.solidArea.y = entity.solidAreaDefaultY; //reset the solid area
                target[i].solidArea.x = target[i].solidAreaDefaultX;
                target[i].solidArea.y = target[i].solidAreaDefaultY;
            }
        }
        return index; //returning index of the object
    }

    /**
     * Checks for collision between an entity and the player.
     * Determines if the entity collides with the player based on its position and direction.
     *
     * @param entity The entity to check for collision.
     * @return True if the entity collides with the player, otherwise returns false.
     */
    public boolean checkPlayer(Entity entity) { //for checking if npc have collision to player

        boolean contactPlayer = false;

        entity.solidArea.x = entity.worldX + entity.solidArea.x;
        entity.solidArea.y = entity.worldY + entity.solidArea.y;
        //get the object's solid area position
        gp.player.solidArea.x = gp.player.worldX + gp.player.solidArea.x;
        gp.player.solidArea.y = gp.player.worldY + gp.player.solidArea.y;
                /* since we have set 0 to the object's solid area's x and y the latter part doesn't
                add anything. but we included that so the code still works even if you set specific values
                in each object */

        switch (entity.direction) {
            //simulating entity's movement and check where it will be after it moved
            case "up":
                entity.solidArea.y -= entity.speed; break;
            case "down":
                entity.solidArea.y += entity.speed; break;
            case "left":
                entity.solidArea.x -= entity.speed; break;
            case "right":
                entity.solidArea.x += entity.speed; break;
        }

        if(entity.solidArea.intersects(gp.player.solidArea)) { //check if entity touches the player
            entity.collisionOn = true;
            contactPlayer = true;
        }

        entity.solidArea.x = entity.solidAreaDefaultX; //reset the solid area
        entity.solidArea.y = entity.solidAreaDefaultY; //reset the solid area
        gp.player.solidArea.x = gp.player.solidAreaDefaultX;
        gp.player.solidArea.y = gp.player.solidAreaDefaultY;

        return contactPlayer;
    }

}
