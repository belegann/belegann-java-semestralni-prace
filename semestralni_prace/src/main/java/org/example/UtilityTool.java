package org.example;

import java.awt.*;
import java.awt.image.BufferedImage;

//CLASS FOR SCALING IMAGES

public class UtilityTool {

    public BufferedImage scaleImage(BufferedImage original, int width, int height) {
        //scale BufferedImage
        BufferedImage scaledImage = new BufferedImage(width, height, original.getType());
        //int width, int height, int imageType // instance BufferedImage, like white canvas
        Graphics2D g2 = scaledImage.createGraphics(); //creates a Graphics2D, which can be used to draw
        //into this BufferedImage (scaledImage)
        g2.drawImage(original, 0, 0, width, height, null);
        //draw tile[0].image into the scaledImage(BuffIm) that this Graphics2D is linked to
        g2.dispose();

        return scaledImage;
    }
}
