package org.example.entity;

import org.example.GamePanel;
import org.example.LoggingManager;
import org.example.UtilityTool;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;

public class Entity {

    GamePanel gp;
    public int worldX, worldY;
    public BufferedImage up1, up2, down1, down2, left1, left2, right1, right2;
    public BufferedImage attackUp1, attackUp2, attackDown1, attackDown2, attackLeft1, attackLeft2, attackRight1, attackRight2;
    public BufferedImage image, image2, image3;
    public String direction = "down";
    protected int spriteNum = 1;
    public int solidAreaDefaultX, solidAreaDefaultY; //for objects interaction
    //for collision
    public Rectangle solidArea = new Rectangle(0, 0, 48, 48); //create invisible/abstract rectangl,
    //default solid area for all entities
    protected Rectangle attackArea = new Rectangle(0, 0, 0, 0); //entity's attack area
    public boolean collisionOn = false; //state
    public boolean collision = false;
    protected boolean invincible = false;  //for player's invincibility then he receives damage
    String dialogues[] = new String[20];

    //COUNTER
    protected  int spriteCounter = 0;
    public int invincibleCounter = 0; //for player's invincibility then he receives damage
    protected int actionLockCounter = 0; //for NORMAL entity's moving
    private int dyingCounter = 0; //for death effect

    private int dialogueIndex = 0;

    protected int shotAvailableCounter = 0; //for fireballs fixing bug

    //CHARACTER ATTRIBUTES /STATUS
    public int maxLife;
    public int life;
    public int speed;
    public String name;


    //************************FOR CHARACTER STATUS************************//
    public int level;
    public int strength;
    public int dexterity;
    public int attack;
    public int defense;
    public int exp;
    public int nextLevelExp;
    public int coin;
    public Entity currentWeapon;
    public Entity currentShield;

    //mana
    public int ammo;                   ///********************* CAN BE ONLY PUBLIC
    public int maxMana;               ///********************* CAN BE ONLY PUBLIC
    public int mana;               ///********************* CAN BE ONLY PUBLIC
    protected Projectile projectile;

    //ITEM ATTRIBUTES
    protected int attackValue;
    protected int defenseValue;
    protected int useCost; //for mana attack, magic
    protected int value;               ///********************* CAN BE ONLY PUBLIC
    protected boolean stackable = false;
    public int amount = 1;               ///********************* CAN BE ONLY PUBLIC

    //type
    public int type; //for knowing if good npc or monster touches player for receiving damage when MONSTER touches player
    //0 - player, 1 - npc, 2 - monster
    private final int type_player = 0; //just for us, we don't use it
    private final int type_npc = 1; //just for us, we don't use it
    protected final int type_monster = 2;
    protected final int type_trident = 3;
    protected final int type_axe = 4;
    protected final int type_shield = 5;
    protected final int type_consumable = 6;
    protected final int type_pickupOnly = 7;
    protected final int type_obstacle = 8;

    public String description = ""; //item description
    //*******************************************************************//


    protected boolean attacking = false; //for attacking
    public boolean alive = true; //for death effect              ///********************* CAN BE ONLY PUBLIC
    public boolean dying = false; //for death effect            ///********************* CAN BE ONLY PUBLIC

    public Entity(GamePanel gp) { //for npc
        this.gp = gp;
    }

    public int getLeftX(){
        return worldX + solidArea.x;
    }
    public int getRightX(){
        return worldX + solidArea.x + solidArea.width;
    }
    public int getTopY(){
        return worldY + solidArea.y;
    }
    public int getBottomY(){
        return worldY + solidArea.y + solidArea.height;
    }

    public int getCol(){
        return ((worldX + solidArea.x)/gp.tileSize);
    }

    public int getRow(){
        return ((worldY + solidArea.y)/gp.tileSize);
    }
    public void setAction() { //for entity moving
    }
    public void damageReaction(){ //for monsters reacting when receives damage
    }

    /**
     * Makes the entity speak by displaying a dialogue.
     */
    public void speak() {
        if(dialogues[dialogueIndex] == null){
            dialogueIndex = 0;
        }
        gp.ui.currentDialogue = dialogues[dialogueIndex];
        dialogueIndex++;

        switch(gp.player.direction) { //we want npc to look at us then talking
            case "up":
                direction = "down";
                break;
            case "down":
                direction = "up";
                break;
            case "left":
                direction = "right";
                break;
            case "right":
                direction = "left";
                break;
        }
    }

    public void interact() {}

    public boolean use(Entity entity) { //override this in player class, for using potion
        return false;
    }

    public void checkDrop(){}

    /**
     * Drops item when monster dies.
     *
     * @param droppedItem The object to be dropped.
     */
    public void dropItem(Entity droppedItem){
        for(int i =0; i < gp.obj.length; i++){
            if(gp.obj[i] == null) {
                gp.obj[i] = droppedItem;
                gp.obj[i].worldX = worldX; //the dead monster's coordinates
                gp.obj[i].worldY = worldY;
                break;
            }
        }
    }

    /**
     * Updates the entity's state, including movement and collision detection.
     */
    public void update() { //for entity moving, checking collisions

        setAction();

        collisionOn = false;
        gp.cChecker.checkTile(this);
        gp.cChecker.checkObject(this, false); //for checking if npc touches player

        //for checking if monster touches player
        gp.cChecker.checkEntity(this, gp.npc);
        gp.cChecker.checkEntity(this, gp.monster);
        gp.cChecker.checkEntity(this, gp.iTile);

        boolean contactPlayer = gp.cChecker.checkPlayer(this); //for checking if npc touches player

        if (this.type == type_monster && contactPlayer == true) { //if monster contacts player
                damagePlayer(attack);
        }

        //IF COLLISION IS FALSE, PLAYER CAN MOVE
        if (collisionOn == false) {
            switch (direction) {
                case "up":
                    worldY -= speed;
                    break;
                case "down":
                    worldY += speed;
                    break;
                case "left":
                    worldX -= speed;
                    break;
                case "right":
                    worldX += speed;
                    break;
            }
        }

        spriteCounter++;
        if (spriteCounter > 12) {
            if (spriteNum == 1) {
                spriteNum = 2;
            } else if (spriteNum == 2) {
                spriteNum = 1;
            }
            spriteCounter = 0;
        }

        if(invincible == true) {
            invincibleCounter++;
            if (invincibleCounter > 40) { //0.6 second, 40 frames
                invincible = false;
                invincibleCounter = 0; //reset the counter
            }
        }
        if (shotAvailableCounter < 30) {
            shotAvailableCounter++;
        }
    }

    /**
     * Damages the player entity with the specified calculated attack power.
     *
     * @param attack The attack power to inflict on the player.
     */
    public void damagePlayer(int attack) {
        if (gp.player.invincible == false) {
            //we can give damage

            int damage = attack - gp.player.defense;
            if(damage < 0){
                damage = 0;
            }
            gp.player.life -= damage;
            gp.player.invincible = true;
        }
    }

    /**
     * Draws the entity on the graphics context.
     *
     * @param g2 Graphics2D.
     */
    public void draw(Graphics2D g2) {

        BufferedImage image = null;

        int screenX = worldX - gp.player.worldX + gp.player.screenX; //moving camera
        int screenY = worldY - gp.player.worldY + gp.player.screenY; //moving camera

        //drawing entity only if we need it
        //add + gp.tileSize for avoiding png "frame"
        if     (worldX + gp.tileSize > gp.player.worldX - gp.player.screenX &&
                worldX - gp.tileSize < gp.player.worldX + gp.player.screenX &&
                worldY + gp.tileSize > gp.player.worldY - gp.player.screenY &&
                worldY - gp.tileSize < gp.player.worldY + gp.player.screenY) {

            switch (direction) {
                case "up":
                    if (spriteNum == 1) {image = up1;}
                    if (spriteNum == 2) {image = up1;}
                    break;
                case "down":
                    if (spriteNum == 1) {image = down1;}
                    if (spriteNum == 2) {image = down2;}
                    break;
                case "left":
                    if (spriteNum == 1) {image = left1;}
                    if (spriteNum == 2) {image = left2;}
                    break;
                case "right":
                    if (spriteNum == 1) {image = right1;}
                    if (spriteNum == 2) {image = right2;}
                    break;
            }

            //Monster HP bar
            if(type == 2) { //entity is a monster

                //for changing health bar when monster receiving damage
                double oneScale = (double)gp.tileSize/maxLife; //oneScale - a little part of health bar(1 life)
                double hpBarValue = oneScale*life;

                //dark grey board of health bar
                g2.setColor(new Color(35, 35, 35));
                g2.fillRect(screenX-1, screenY-16, gp.tileSize+2, 12);

                g2.setColor(new Color(255, 0, 30));
                g2.fillRect(screenX, screenY - 15, (int)hpBarValue, 10);
            }

            if(invincible == true) { //monster has lower opacity when we damage him
                g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f));
            }

            if(dying) {
                dyingAnimation(g2);
            }

            g2.drawImage(image, screenX, screenY, null);
            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
        }
    }

    /**
     * Performs the dying animation effect of the monster(entity).
     *
     * @param g2 Graphics2D.
     */
    public void dyingAnimation(Graphics2D g2) { //for dying effect of enemy, blinking effect
        dyingCounter++;
        //in every 5 frames we switch the monster's opacity
        int i = 5;
        if(dyingCounter <= i) {changeOpacity(g2, 0f);}
        if(dyingCounter > i && dyingCounter <= i*2) {changeOpacity(g2, 1f);}
        if(dyingCounter > i*2 && dyingCounter <= i*3) {changeOpacity(g2, 0f);}
        if(dyingCounter > i*3 && dyingCounter <= i*4) {changeOpacity(g2, 1f);}
        if(dyingCounter > i*4 && dyingCounter <= i*5) {changeOpacity(g2, 0f);}
        if(dyingCounter > i*5 && dyingCounter <= i*6) {changeOpacity(g2, 1f);}
        if(dyingCounter > i*6 && dyingCounter <= i*7) {changeOpacity(g2, 0f);}
        if(dyingCounter > i*8 && dyingCounter <= i*9) {changeOpacity(g2, 1f);}
        if(dyingCounter >  i*9) {

            alive = false;
        }
    }

    public void changeOpacity(Graphics2D g2, float alphaValue) { //for blinking (death) effect
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alphaValue));
    }

    /**
     * Sets up and returns a scaled BufferedImage based on the provided image path, width, and height.
     *
     * @param imagePath The path to the image file (excluding the file extension) to be loaded.
     * @param width     The desired width of the scaled image.
     * @param height    The desired height of the scaled image.
     * @return A BufferedImage object representing the loaded and scaled image, or null if an error occurred.
     */
    public BufferedImage setup(String imagePath, int width, int height) { //method for avoiding lots of same code for entity's images
        UtilityTool uTool = new UtilityTool();
        BufferedImage image = null;

        try {

            image = ImageIO.read(new FileInputStream( imagePath + ".png"));
            image = uTool.scaleImage(image, width, height);
        }
        catch(IOException e){
            LoggingManager.getLogger().severe("Game loop interrupted: " + e.getMessage());
        }
        return image;
    }


    /**
     * Returns the index of a detected target entity object  to the user in the game world. This method is used for opening the chest
     * with key - fish (OBJ_Fish), but I don't use it
     * @param user        The user entity - player.
     * @param target      An array of target entities - objects.
     * @param targetName  The name of the target entity - object.
     * @return The index of the detected target entity-object in the target array. If the target entity is not found, returns 999.
     */
    public int getDetected(Entity user, Entity target[], String targetName) {

        int index = 999;

        //Check the surrounding object
        int nextWorldX = user.getLeftX();
        int nextWorldY = user.getTopY();

        switch(user.direction){
            case"up": nextWorldY= user.getTopY()-1; break;
            case"down": nextWorldY= user.getBottomY()+1; break;
            case"left": nextWorldX = user.getLeftX()-1; break;
            case"right": nextWorldX= user.getRightX()+1; break;
        }

        int col = nextWorldX/gp.tileSize;
        int row = nextWorldY/gp.tileSize;

        for (int i = 0; i < target.length; i++){
            if(target[i] != null){
                if(target[i].getCol() == col &&
                        target[i].getRow() == row &&
                        target[i].name.equals(targetName)) {
                    index = i;
                    break;
                }
            }
        }
        return index;

    }

}
