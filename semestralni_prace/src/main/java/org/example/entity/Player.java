package org.example.entity;

import org.example.object.*;
import org.example.GamePanel;
import org.example.Keyboard;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Player extends Entity{

    //GamePanel gp;
    Keyboard keyH;

    public final int screenX; //for camera
    public final int screenY;
    public ArrayList<Entity> inventory = new ArrayList<>(); //for inventory
    public final int maxInventorySize = 20;
    public int amount = 1;

    //public int hasKey = 0; //for objects interaction - key(fish)

    public Player(GamePanel gp, Keyboard keyH) {

        super(gp); //passing gp to Entity class
        //this.gp = gp;
        this.keyH = keyH;

        screenX = gp.screenWidth/2 - (gp.tileSize/2); //displaying player at the center
        screenY = gp.screenHeight/2 - (gp.tileSize/2); // - subtract a half tile length from screenX and screenY

        solidArea = new Rectangle(); //for solid objects
        solidArea.x = 8; //not all player's tile is solid
        solidArea.y = 16;
        solidAreaDefaultX = solidArea.x; //for objects interaction
        solidAreaDefaultY = solidArea.y; //for objects interaction
        solidArea.width = 32;
        solidArea.height = 32;

        /* attackArea.width = 36; //attack area
        attackArea.height = 36; */

        setDefaultValues();

    }

    public void setDefaultValues() {

        worldX = gp.tileSize * 4; //player's start position
        //worldX = gp.tileSize * 3 - (gp.tileSize/2);
        worldY = gp.tileSize * 4; //8*8
        speed = 4;
        direction = "down";

        //PLAYER STATUS
        level = 1;
        strength = 1; //The more strength he has, the more damage he gives
        dexterity = 1; // The more dexterity he has, the less damage he receives
        exp = 0;
        nextLevelExp = 3; //how much player need exp to level up
        coin = 0;
        currentWeapon = new OBJ_weapon_basic(gp);
        currentShield = new OBJ_Shield_basic(gp);
        projectile = new OBJ_Fireball(gp); //mana
        attack = getAttack(); // The total attack value is decided by strength and weapon
        defense = getDefense(); // The total defence value is decided by dexterity and shield
        maxLife = 6; //1 life means half_cat, 2 lifes means full_cat, so there would be three cats
        life = maxLife;
        maxMana = 4;
        mana = maxMana;
        ammo = 10;


        getPlayerImage();
        getPlayerAttackImage();
        setItems();
    }

    public void setDefaultPositions() {
        worldX = gp.tileSize * 4;
        worldY = gp.tileSize * 4;
        direction = "down";
    }

    public void restoreStatus(){

        life = maxLife;
        mana = maxMana;
        invincible = false;
        attacking = false;
    }

    public void setItems(){

        inventory.clear();
        inventory.add(currentWeapon);
        inventory.add(currentShield);
        //inventory.add(new OBJ_Key(gp));
    }

    public int getAttack(){
        attackArea = currentWeapon.attackArea;
        return attack = strength * currentWeapon.attackValue;
    }
    public int getDefense(){
        return defense = dexterity * currentShield.defenseValue;
    }

    /**
     * Retrieves the slot index of the player's current weapon in the inventory.
     *
     * @return The slot index of the current weapon. Returns 0 if the current weapon is not found in the inventory.
     */
    public int getCurrentWeaponSlot(){
        int currentWeaponSlot = 0;
        for (int i = 0; i < inventory.size(); i++){
            if(inventory.get(i) == currentWeapon){
                currentWeaponSlot = i;
            }
        }
        return currentWeaponSlot;
    }

    /**
     * Retrieves the slot index of the player's current shield in the inventory.
     *
     * @return The slot index of the current shield. Returns 0 if the current shield is not found in the inventory.
     */
    public int getCurrentShieldSlot(){
        int currentShieldSlot = 0;
        for (int i = 0; i < inventory.size(); i++){
            if(inventory.get(i) == currentShield){
                currentShieldSlot = i;
            }
        }
        return currentShieldSlot;
    }

    public void getPlayerImage() {

            /*LOADING IMAGES*/

            //up1 = ImageIO.read(new FileInputStream("res/player/up1.png"));

            up1 = setup("res/player/up1", gp.tileSize, gp.tileSize);
            up2 = setup("res/player/up2", gp.tileSize, gp.tileSize);

            down1 = setup("res/player/down1", gp.tileSize, gp.tileSize);
            down2 = setup("res/player/down2", gp.tileSize, gp.tileSize);

            left1 = setup("res/player/left1", gp.tileSize, gp.tileSize);
            left2 = setup("res/player/left2", gp.tileSize, gp.tileSize);

            right1 = setup("res/player/right1", gp.tileSize, gp.tileSize);
            right2 = setup("res/player/right2", gp.tileSize, gp.tileSize);

    }

    public void getPlayerAttackImage() { //for getting attack images, because we can have different weapon

        if(currentWeapon.type == type_trident) {
            attackUp1 = setup("res/player/attack_trident_up_1", gp.tileSize, gp.tileSize * 2);
            attackUp2 = setup("res/player/attack_trident_up_2", gp.tileSize, gp.tileSize * 2);
            attackDown1 = setup("res/player/attack_trident_down_1", gp.tileSize, gp.tileSize * 2);
            attackDown2 = setup("res/player/attack_trident_down_2", gp.tileSize, gp.tileSize * 2);
            attackLeft1 = setup("res/player/attack_trident_left_1", gp.tileSize * 2, gp.tileSize);
            attackLeft2 = setup("res/player/attack_trident_left_2", gp.tileSize * 2, gp.tileSize);
            attackRight1 = setup("res/player/attack_trident_right_1", gp.tileSize * 2, gp.tileSize);
            attackRight2 = setup("res/player/attack_trident_right_2", gp.tileSize * 2, gp.tileSize);
        }

        if(currentWeapon.type == type_axe) {
            attackUp1 = setup("res/player/attack_axe_up_1", gp.tileSize, gp.tileSize * 2);
            attackUp2 = setup("res/player/attack_axe_up_2", gp.tileSize, gp.tileSize * 2);
            attackDown1 = setup("res/player/attack_axe_down_1", gp.tileSize, gp.tileSize * 2);
            attackDown2 = setup("res/player/attack_axe_down_2", gp.tileSize, gp.tileSize * 2);
            attackLeft1 = setup("res/player/attack_axe_left_1", gp.tileSize * 2, gp.tileSize);
            attackLeft2 = setup("res/player/attack_axe_left_2", gp.tileSize * 2, gp.tileSize);
            attackRight1 = setup("res/player/attack_axe_right_1", gp.tileSize * 2, gp.tileSize);
            attackRight2 = setup("res/player/attack_axe_right_2", gp.tileSize * 2, gp.tileSize);
        }

    }


    /**
     * Updates the player's state.
     */
    public void update() {

        if (attacking) {
            attacking();
        }
        else if (keyH.rightPressed == true || keyH.downPressed == true || keyH.leftPressed == true || keyH.upPressed == true
                || keyH.enterPressed == true) {
            if (keyH.upPressed == true) {
                direction = "up";
            } else if (keyH.downPressed == true) {
                direction = "down";
            } else if (keyH.leftPressed == true) {
                direction = "left";
            } else if (keyH.rightPressed == true) {
                direction = "right";
            }

            //CHECK TILE COLLISION
            collisionOn = false;
            gp.cChecker.checkTile(this);

            //CHECK OBJECT COLLISION
            int objIndex = gp.cChecker.checkObject(this, true);
            pickUpObject(objIndex);

            //CHECK NPC COLLISION
            int npcIndex = gp.cChecker.checkEntity(this, gp.npc);
            interactNPC(npcIndex);

            //CHECK EVENT COLLISION
            gp.eHandler.checkEvent();

            //CHECK MONSTER COLLISION
            int monsterIndex = gp.cChecker.checkEntity(this, gp.monster);
            contactMonster(monsterIndex); //for taking damage

            //CHECK INTERACTIVE TILES COLLISION
            int iTileIndex = gp.cChecker.checkEntity(this, gp.iTile);

            //IF COLLISION IS FALSE, PLAYER CAN MOVE
            if (collisionOn == false && keyH.enterPressed == false) {
                switch (direction) {
                    case "up":
                        worldY -= speed;
                        break;
                    case "down":
                        worldY += speed;
                        break;
                    case "left":
                        worldX -= speed;
                        break;
                    case "right":
                        worldX += speed;
                        break;
                }
            }
            gp.keyH.enterPressed = false;

            spriteCounter++;
            if (spriteCounter > 12) {
                if (spriteNum == 1) {
                    spriteNum = 2;
                } else if (spriteNum == 2) {
                    spriteNum = 1;
                }
                spriteCounter = 0;
            }
        }
        if(gp.keyH.shotKeyPressed && projectile.alive == false
                && shotAvailableCounter == 30 && projectile.haveResource(this)){

            projectile.set(worldX, worldY, direction, true, this); //setting the default coordinates, direction and user

            //subtract the cost(mana)
            projectile.subtractResource(this);

            //add it to a list
            gp.projectileList.add(projectile);
            shotAvailableCounter = 0;
        }

        //for invincibility
        if(invincible == true) {
            invincibleCounter++;
            if (invincibleCounter > 60) { //1 second, 60 frames
                invincible = false;
                invincibleCounter = 0; //reset the counter
            }
        }

        if (shotAvailableCounter < 30){ //cannot draw another fireball for the next 30 frames
            shotAvailableCounter++;
        }

        if (life > maxLife){
            life = maxLife;
        }
        if (mana > maxMana){
            mana = maxMana;
        }
        //for ending game
        if(life <= 0){
            gp.gameState = gp.gameOverState;
        }

    }

    /**
     * Player picks up an object at the specified index.
     *
     * @param i The index of the object to pick up.
     */
    /* WHAT SHOULD HAPPEN WHEN PLAYER TOUCHES OBJECT ??? METHOD */
    public void pickUpObject(int i) { //method for picking up object
        if (i != 999) {

            //pick only items
            if(gp.obj[i].type == type_pickupOnly){

                gp.obj[i].use(this);
                gp.obj[i] = null;
            }
            //OBSTACLE
            else if(gp.obj[i].type == type_obstacle){
                if(keyH.enterPressed){
                    //attackCanceled = true;
                    gp.obj[i].interact();
                }
            }
            //inventory items
            else{
                String text;

                if(canObtainItem(gp.obj[i])) { //checking if inventory is full

                    //inventory.add(gp.obj[i]);
                    text = "Got a " + gp.obj[i].name + "!";

                }
                else {
                    text = "Your inventory is full!";
                }
                gp.ui.addMessage(text);
                gp.obj[i] = null;
            }

        }
    }

    /**
     * Performs the player's attack animation.
     * Updates the sprite and checks for collisions with monsters and interactive tiles during the attack.
     */
    public void attacking(){ //player attack animation
        spriteCounter++;

        if(spriteCounter <= 5) { //show attack image 1 during the first 5 frames
            spriteNum = 1;
        }

        if(spriteCounter > 5 && spriteCounter <= 25) { //show image 2 6-25 frames
            spriteNum = 2;

            //save the current worldX, worldY, solid Area
            int currentWorldX = worldX;
            int currentWorldY = worldY;
            int solidAreaWidth = solidArea.width;
            int solidAreaHeight = solidArea.height;

            //Adjust player's worldX/Y for the attackArea
            switch(direction) {
                case "up": worldY -= attackArea.height; break; //player' coordinates
                case "down": worldY += attackArea.height; break;
                case "left": worldX -= attackArea.width; break;
                case "right": worldX += attackArea.width; break;
            }

            //attack area become solidArea
            solidArea.width = attackArea.width;
            solidArea.height = attackArea.height;
            //check monster collision with the updated worldX, worldY, solidArea
            int monsterIndex = gp.cChecker.checkEntity(this, gp.monster);
            damageMonster(monsterIndex, attack);

            //INTERACTIVE TILE
            int iTileIndex = gp.cChecker.checkEntity(this, gp.iTile);
            damageInteractiveTile(iTileIndex);

            //after checking coliision, restore the original data
            worldX = currentWorldX;
            worldY = currentWorldY;
            solidArea.width = solidAreaWidth;
            solidArea.height = solidAreaHeight;

        }
        if(spriteCounter > 25) { //finish attacking animation
            spriteNum = 1;
            spriteCounter = 0;
            attacking = false;
        }
    }

    /**
     * Interacts with an NPC if there is no collision, (talk).
     *
     * @param i The index of the NPC to interact with.
     */
    public void interactNPC(int i) { //player interacts with NPC if there is no collision
        if (i != 999) { //player is touching npc and interact
            //System.out.println("You are hitting the NPC!!!!");
                gp.gameState = gp.dialogueState;
                gp.npc[i].speak();
        }
        else {
            if(gp.keyH.enterPressed == true) { //attacking
                attacking = true;
            }
        }
    }

    /**
     * Handles the player's contact with a monster, causing damage to the player if not invincible.
     *
     * @param i The index of the monster the player is in contact with.
     */
    public void contactMonster(int i){
        if (i != 999) {

            if (invincible == false && gp.monster[i].dying == false) { //player receieves damage only if he is not invincible

                int damage = gp.monster[i].attack - defense;
                if(damage < 0){
                    damage = 0;
                }

                life -= damage;
                invincible = true;
            }
        }
    }

    /**
     * Damages a monster with the specified attack value, reducing its life and triggering reactions.
     *
     * @param i      The index of the monster to be damaged.
     * @param attack The attack value to be used for damaging the monster.
     */
    public void damageMonster(int i, int attack){ //for damaging monster
        if(i != 999) {
           if(!gp.monster[i].invincible) {

               int damage = attack - gp.monster[i].defense;
               if(damage < 0){
                   damage = 0;
               }

               gp.monster[i].life -= damage;

               gp.ui.addMessage(damage + " damage!"); //message

               gp.monster[i].invincible = true;
               gp.monster[i].damageReaction();

               if(gp.monster[i].life <= 0) { //moster dies
                   gp.monster[i].dying = true; //dying state is true
                   gp.ui.addMessage("Killed the " + gp.monster[i].name + "!");
                   gp.ui.addMessage("Exp + " + gp.monster[i].exp + "!");
                   exp += gp.monster[i].exp;
                   checkLevelUp();
               }
           }
        }
    }

    /**
     * Damages an interactive tile, reducing its life and destroying it.
     *
     * @param i The index of the interactive tile to be damaged.
     */
    public void damageInteractiveTile(int i){
        if(i != 999 && gp.iTile[i].destructible
                && gp.iTile[i].isCorrectItem(this) && !gp.iTile[i].invincible){
            gp.iTile[i].life--;
            gp.iTile[i].invincible = true;

            if(gp.iTile[i].life == 0){
                gp.iTile[i] = gp.iTile[i].getDestroyedRForm();
            }

        }
    }
    public void checkLevelUp() {

        if(exp >= nextLevelExp) {
            level++;
            nextLevelExp = nextLevelExp * 2;
            maxLife += 2;
            strength++;
            dexterity++;
            attack = getAttack(); //recalculating attack
            defense = getDefense(); //recalculating defense

            gp.gameState = gp.dialogueState;
            gp.ui.currentDialogue = "You are level " + level + " now!\n";
        }
    }

    /**
     * Selects an item from the inventory and performs the corresponding actions based on the item's type.
     * The actions may include equipping weapons or shields, using consumable items.
     */
    public void selectItem() { //from inventory

        int itemIndex = gp.ui.getItemIndexOnSlot();

        if(itemIndex < inventory.size()) {
            Entity selectedItem  = inventory.get(itemIndex);

            if(selectedItem.type == type_trident || selectedItem.type == type_axe) {
                currentWeapon = selectedItem;
                attack = getAttack();
                getPlayerAttackImage();
            }
            if(selectedItem.type == type_shield) {
                currentShield = selectedItem;
                defense = getDefense();
            }
            if(selectedItem.type == type_consumable) {

                if(selectedItem.use(this)){
                    if(selectedItem.amount > 1) {
                        selectedItem.amount--;
                    }
                    else {
                        inventory.remove(itemIndex);
                    }

                }

            }
        }
    }

    public int searchItemInInventory(String itemName) {
        int itemIndex = 999;

        for (int i = 0; i < inventory.size(); i++) {
            if (inventory.get(i).name.equals(itemName)) {
                itemIndex = i;
                break;
            }
        }
        return itemIndex;
    }

    public boolean canObtainItem(Entity item){ //check if we can obtain this item

        boolean canObtain = false;

        //Check if item is stackable
        if(item.stackable){
            int index = searchItemInInventory(item.name);

            if(index != 999){
                inventory.get(index).amount++;
                canObtain = true;
            }
            else{ //new item so need to check vacancy
                if(inventory.size() !=  maxInventorySize){
                    inventory.add(item);
                    canObtain = true;
                }
            }
        }
        else { //not stackable so check vacancy
            if(inventory.size() !=  maxInventorySize){
                inventory.add(item);
                canObtain = true;
            }
        }
        return canObtain;
    }

    /**
     * Draws the player character on the screen using the provided graphics context.
     *
     * @param g2 Graphics2D.
     */
    public void draw(Graphics2D g2) {
        //g2.setColor(Color.white);  //setting color on a character
        //g2.fillRect(x, y, gp.tileSize, gp.tileSize);  //draw a rectangle and fills ut with white color

        BufferedImage image = null;
        int tempScreenX = screenX; //for normal player position then attacking
        int tempScreenY = screenY;
        switch (direction) {
            case "up":
                if(!attacking){
                    if (spriteNum == 1) {image = up1;}
                    if (spriteNum == 2) {image = up2;}
                }
                if(attacking){
                    tempScreenY = screenY - gp.tileSize;
                    if (spriteNum == 1) {image = attackUp1;}
                    if (spriteNum == 2) {image = attackUp2;}
                }
                break;
            case "down":
                if(!attacking){
                    if (spriteNum == 1) {image = down1;}
                    if (spriteNum == 2) {image = down2;}
                }
                if(attacking){
                    if (spriteNum == 1) {image = attackDown1;}
                    if (spriteNum == 2) {image = attackDown2;}
                }
                break;
            case "left":
                if(!attacking) {
                    if (spriteNum == 1) {image = left1;}
                    if (spriteNum == 2) {image = left2;}
                }
                if(attacking){
                    tempScreenX = screenX - gp.tileSize;
                    if (spriteNum == 1) {image = attackLeft1;}
                    if (spriteNum == 2) {image = attackLeft2;}
                }
                break;
            case "right":
                if(!attacking) {
                    if (spriteNum == 1) {image = right1;}
                    if (spriteNum == 2) {image = right2;}
                }
                if(attacking){
                    if (spriteNum == 1) {image = attackRight1;}
                    if (spriteNum == 2) {image = attackRight2;}
                }
                break;
        }

        if(invincible == true) { //player has lower opacity when he is invincible
            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.4f));
        }

        /* DRAW AN IMAGE ON THE SCREEN */
        g2.drawImage(image, tempScreenX, tempScreenY, null);

        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f)); //reset opacity

    }

}
