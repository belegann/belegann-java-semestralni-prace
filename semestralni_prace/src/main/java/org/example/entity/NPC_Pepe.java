package org.example.entity;

import org.example.GamePanel;

import java.util.Random;


public class NPC_Pepe extends Entity{

    public NPC_Pepe(GamePanel gp) {

        super(gp);
        direction = "down";
        speed = 3;
        getImage();
        setDialogue();

        solidArea.width = 48*2;
        solidArea.height = 48*2;
        solidAreaDefaultX = solidArea.x;
        solidAreaDefaultY = solidArea.y;
    }

    public void getImage() { //getting pictures of pepe

        up1 = setup("res/npc/pepe_up_1", gp.tileSize*2, gp.tileSize*2);
        up2 = setup("res/npc/pepe_up_1", gp.tileSize*2, gp.tileSize*2);

        down1 = setup("res/npc/pepe_down_1", gp.tileSize*2, gp.tileSize*2);
        down2 = setup("res/npc/pepe_down_2", gp.tileSize*2, gp.tileSize*2);

        left1 = setup("res/npc/pepe_left_1", gp.tileSize*2, gp.tileSize*2);
        left2 = setup("res/npc/pepe_left_2", gp.tileSize*2, gp.tileSize*2);

        right1 = setup("res/npc/pepe_right_1", gp.tileSize*2, gp.tileSize*2);
        right2 = setup("res/npc/pepe_right_2", gp.tileSize*2, gp.tileSize*2);

    }

    public void setDialogue() { //creating dialogue with pepe
        dialogues[0] = "Hi!";
        dialogues[1] = "I am Pepe The Frog!";
    }

    public void setAction() { //set character's behavior, pepe must move here

        actionLockCounter++;

        if(actionLockCounter == 120) {
            Random random = new Random();
            int i = random.nextInt(100)+1; //from 0 to 100 - random

            if(i <= 25 ) {
                direction = "up";
            }

            if(i > 25  && i <= 50) {
                direction = "down";
            }

            if(i > 50 && i <= 75 ) {
                direction = "left";
            }

            if(i > 75 && i <= 100) {
                direction = "right";
            }
            actionLockCounter = 0;
        }
    }

    public void speak() { //speak with player
        super.speak(); //calling the method from entity class
    }
}
