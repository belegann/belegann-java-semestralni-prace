package org.example.data;

import java.io.Serializable;
import java.util.ArrayList;

public class DataStorage implements Serializable { //this class itself can be writtable and readable

    //declare whatever variables we want to save and load

    //PLAYER
    int level;
    int maxLife;
    int life;
    int mana;
    int maxMana;
    int strength;
    int dexterity;
    int exp;
    int nextLevelExp;
    int coin;

    //PLAYER INVENTORY
    ArrayList<String> itemNames = new ArrayList<>();
    ArrayList<String> itemAmounts = new ArrayList<>();
    int currentWeaponSlot;
    int currentShieldSlot;

    //OBJECT  ON MAP
    String mapObjectNames[];
    int mapObjectWorldX[];
    int mapObjectWorldY[];
    String mapObjectLootNames[];

}
