package org.example.data;

import org.example.GamePanel;
import org.example.LoggingManager;
import org.example.entity.Entity;
import org.example.object.*;

import java.io.*;

public class SaveLoad {

    GamePanel gp;

    public SaveLoad(GamePanel gp){
        this.gp = gp;
    }


    /**
     * Create the Entity object corresponding to the specified item name.
     *
     * @param itemName The name of the item to retrieve.
     * @return The Entity object corresponding to the item name.
     */
    public Entity getObject (String itemName){
        Entity obj = null;

        switch(itemName) {
            case "Forest Axe": obj = new OBJ_Axe(gp); break;
            case "Boots": obj = new OBJ_boots(gp); break;
            case "Chest": obj = new OBJ_chest(gp); break;
            case "Fish": obj = new OBJ_Key(gp); break;
            case "Gold Potion": obj = new OBJ_Potion_Gold(gp); break;
            case "Shield": obj = new OBJ_Shield_basic(gp); break;
            case "Forest Shield": obj = new OBJ_Shield_forest(gp); break;
            case "Trident": obj = new OBJ_weapon_basic(gp); break;
        }
        return obj;
    }

    /**
     * Saves the game state to a file.
     * Writes the game data and player information to the file save.dat.
     */
    public void save(){
        try{
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("save.dat")));

            DataStorage ds = new DataStorage();

            //pass our game data to this storage-class
            //PLAYER STATUS
            ds.level = gp.player.level;
            ds.maxLife = gp.player.maxLife;
            ds.life = gp.player.life;
            ds.maxMana = gp.player.maxMana;
            ds.mana = gp.player.mana;
            ds.strength = gp.player.strength;
            ds.dexterity = gp.player.dexterity;
            ds.exp = gp.player.exp;
            ds.nextLevelExp = gp.player.nextLevelExp;
            ds.coin = gp.player.coin;

            //PLAYER INVENTORY
            for(int i = 0; i < gp.player.inventory.size(); i++){
                ds.itemNames.add(gp.player.inventory.get(i).name);
                String x = String.valueOf(gp.player.inventory.get(i).amount);
                ds.itemAmounts.add(x);
            }

            //PLAYER EQUIPMENT
            ds.currentWeaponSlot = gp.player.getCurrentWeaponSlot();
            ds.currentShieldSlot = gp.player.getCurrentShieldSlot();

            //OBJECTS ON MAP
            ds.mapObjectNames = new String[gp.obj.length];
            ds.mapObjectWorldX = new int[gp.obj.length];
            ds.mapObjectWorldY = new int[gp.obj.length];
            ds.mapObjectLootNames = new String[gp.obj.length];

            for (int i = 0; i < gp.obj.length; i++){
                if(gp.obj[i] == null){
                    ds.mapObjectNames[i] = "NA";
                }
                else{
                    ds.mapObjectNames[i] = gp.obj[i].name;
                    ds.mapObjectWorldX[i] = gp.obj[i].worldX;
                    ds.mapObjectWorldY[i] = gp.obj[i].worldY;

                }
            }

            //Write the DataStorage object
            oos.writeObject(ds);

        }
        catch(Exception e){
            LoggingManager.getLogger().severe("Save Exception");
        }

    }


    /**
     * Loads the game state from a file.
     * Reads the game data and player information from a save.dat file.
     */
    public void load(){

        try{
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("save.dat")));
            //Read the DataStorage object
            DataStorage ds = (DataStorage)ois.readObject();

            //PLAYER STATUS
             gp.player.level = ds.level;
             gp.player.maxLife = ds.maxLife;
             gp.player.life = ds.life;
             gp.player.maxMana = ds.maxMana;
             gp.player.mana = ds.mana;
             gp.player.strength = ds.strength;
             gp.player.dexterity = ds.dexterity;
             gp.player.exp = ds.exp;
             gp.player.nextLevelExp = ds.nextLevelExp;
             gp.player.coin = ds.coin;

             //PLAYER INVENTORY
             gp.player.inventory.clear();
             for(int i = 0; i < ds.itemNames.size(); i++){
                 gp.player.inventory.add(getObject(ds.itemNames.get(i)));

                Integer x = Integer.parseInt(ds.itemAmounts.get(i));
                gp.player.inventory.get(i).amount = x;
             }

            //PLAYER EQUIPMENT
            gp.player.currentWeapon = gp.player.inventory.get(ds.currentWeaponSlot);
            gp.player.currentShield = gp.player.inventory.get(ds.currentShieldSlot);
            gp.player.getAttack();
            gp.player.getDefense();
            gp.player.getPlayerAttackImage();

            //OBJECTS ON MAP
            for (int i = 0; i < gp.obj.length; i++){
                if(ds.mapObjectNames[i].equals("NA")){  
                     gp.obj[i] = null;
                }
                else {
                    gp.obj[i] = getObject(ds.mapObjectNames[i]);
                    gp.obj[i].worldX = ds.mapObjectWorldX[i];
                    gp.obj[i].worldY = ds.mapObjectWorldY[i];
                }
            }
        }
        catch (Exception e) {LoggingManager.getLogger().severe("Load Exception");;}

    }
}
