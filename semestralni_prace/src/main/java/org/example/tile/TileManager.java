package org.example.tile;

import org.example.GamePanel;
import org.example.LoggingManager;
import org.example.UtilityTool;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.Buffer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TileManager {

    GamePanel gp;
    public Tile [] tile;
    public int mapTileNum[][];

    public TileManager(GamePanel gp) {
        this.gp = gp;
        tile = new Tile[40]; //create 40 kinds of tile
        mapTileNum = new int [gp.maxWorldCol][gp.maxWorldRow];
        getTileImage();
        loadMap("res/mapy/world.txt");
    }

    /**
     * Sets up the tile images.
     * Each tile is associated with an index, name (for path), and collision property.
     * The images are loaded from files and scaled to the desired size.
     */
    public void getTileImage() { //setting up tiles
        setup(0, "woodfloor", false);
        setup(1, "wall", true);
        setup(2, "pivo", true);
        setup(3, "lava", false);
        setup(4, "sofa", true);
        setup(5, "colomn", true);
        setup(6, "goldstatue", true);
        setup(7, "pantheon1", false);
        setup(8, "pantheon2", false);
        setup(9, "pantheon3", false);
        setup(10, "milk", false);
        setup(11, "hall1", false);
        setup(12, "gardenwall", true);
        setup(14, "forest", false);
        //FOREST
        setup(15, "forestspruce", true);
        setup(16, "foresttree1", true);
        setup(17, "foresttree2", true);
        setup(18, "greenpath", false);
        setup(19, "greenpath2", false);
        setup(20, "brownpathstraignt", false);
        setup(21, "squarerightupcorner", false);
        setup(22, "brownpathhorizontal", false);
        setup(23, "squareleftupcorner", false);
        setup(24, "floor", false);
        setup(25, "crosspath", false);
        setup(26, "squareleftdowncorner", false);
    }

    /**
     * Sets up a specific tile at the given index.
     * It loads the image from folders for the tile, scales it, and sets the collision property.
     *
     * @param index     The index of the tile.
     * @param imageName The name of the image file for the tile.
     * @param collision The collision property of the tile.
     */
    public void setup(int index, String imageName, boolean collision) { //method for tiles to avoid all dublicated lines
        UtilityTool uTool = new UtilityTool();
        try{
            tile[index] = new Tile();
            tile[index].image = ImageIO.read(new FileInputStream("res/tiles/"+ imageName + ".png"));
            tile[index].image = uTool.scaleImage(tile[index].image, gp.tileSize, gp.tileSize);
            tile[index].collision = collision;
        }
        catch(IOException e){
            LoggingManager.getLogger().severe("Failed to load image for tile at index: " + index);
            e.printStackTrace();
        }
    }

    /**
     * Loads the game map from a text file.
     * The tile numbers are stored in the mapTileNum array.
     *
     * @param filePath The path to the text file containing the map data.
     */
    public void loadMap(String filePath){ //function for loading map from txt document

        try {

            InputStream is = new FileInputStream(filePath);
            BufferedReader br = new BufferedReader(new InputStreamReader(is)); //read the context of txt file

            int col = 0; //col of world
            int row = 0; //row of world

            while (col < gp.maxWorldCol && row < gp.maxWorldRow) {
                String line = br.readLine(); //read a line of text

                while(col < gp.maxWorldCol) {
                    String numbers[] = line.split(" "); //splits this string at a space
                    int num = Integer.parseInt(numbers[col]); //col is an index of numbers array
                    mapTileNum[col][row] = num;
                    col++; //continue until everything in the numbers[] is stored in the mapTileNum[][]
                }
                if (col == gp.maxWorldCol) {
                    col = 0;
                    row++;
                }
            }
            br.close();

        } catch(Exception e)  {
            LoggingManager.getLogger().severe("Failed to load map from file: " + filePath);
            e.printStackTrace();
        }
    }


    /**
     * Draws the tiles on the screen.
     * It iterates over the mapTileNum array and draws the corresponding tile image.
     *
     * @param g2 The Graphics2D object.
     */
    public void draw(Graphics2D g2) {

        /*AUTOMATIC TILE DRAWING PROCESS*/

        int col = 0; //col of world
        int row = 0; //rows of world

        while(col < gp.maxWorldCol && row < gp.maxWorldRow) {

            int tileNum = mapTileNum[col][row]; //extract a tile number\

            int worldX = col * gp.tileSize;
            int worldY = row * gp.tileSize;
            int screenX = worldX - gp.player.worldX + gp.player.screenX; //moving camera
            int screenY = worldY - gp.player.worldY + gp.player.screenY; //moving camera

            //drawing tile only if we need it
            //add + gp.tileSize for avoiding png "frame"
            if(worldX + gp.tileSize > gp.player.worldX - gp.player.screenX &&
            worldX - gp.tileSize < gp.player.worldX + gp.player.screenX &&
            worldY + gp.tileSize > gp.player.worldY - gp.player.screenY &&
            worldY - gp.tileSize < gp.player.worldY + gp.player.screenY) {
                g2.drawImage(tile[tileNum].image, screenX, screenY, gp.tileSize, gp.tileSize, null);
            }

            //drawing tiles one by one
            col++;
            //x += gp.tileSize;

            if (col == gp.maxWorldCol) {
                col = 0;
                //x = 0;
                row++;
                //y+= gp.tileSize;
            }
        }
    }
}
