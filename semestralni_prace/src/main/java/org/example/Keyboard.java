package org.example;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/*INTERFACE FOR RECEIVING KEYBOARD EVENTS (KEYSTROKES)*/
public class Keyboard implements KeyListener {

    public GamePanel gp;
    public boolean upPressed, downPressed, leftPressed, rightPressed, enterPressed;
    public boolean shotKeyPressed; //for mana attack(fireball)
    public Keyboard(GamePanel gp) {
        this.gp = gp;
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    /**
     * Handles the event when a key is pressed.
     *
     * @param e The KeyEvent representing the key press event.
     */
    @Override
    public void keyPressed(KeyEvent e) {  //key is being pressed
        int code = e.getKeyCode(); //returns the integer keyCode associated with the key in this event

        //FOR MENU, TITLE STATE
        if(gp.gameState == gp.titleState){
            titleState(code);
        }
        //PLAY STATE
        else if(gp.gameState == gp.playState) {
            playState(code);
        }
        //PAUSE STATE
        else if (gp.gameState == gp.pauseState) {
            pauseState(code);
        }

        //DIALOGUE STATE
        else if(gp.gameState == gp.dialogueState) {
            dialogueState(code);
        }

        //CHARACTER STATUS(STATE)
        else if (gp.gameState == gp.characterState) {
            characterState(code);
        }

        //OPTIONS STATE
        else if (gp.gameState == gp.optionsState) {
            optionsState(code);
        }

        //GAME OVER STATE
        else if (gp.gameState == gp.gameOverState) {
            gameOverState(code);
        }
    }


    /**
     * Handles key presses in the title state.
     *
     * @param code the keycode of the pressed key
     */
    public void titleState(int code){
        if ((code == KeyEvent.VK_W) || (code == KeyEvent.VK_UP)) {
            gp.ui.commandNum--;
            if(gp.ui.commandNum < 0) {
                gp.ui.commandNum = 2;
            }
        }

        if ((code == KeyEvent.VK_S) || (code == KeyEvent.VK_DOWN)) {
            gp.ui.commandNum++;
            if(gp.ui.commandNum > 2) {
                gp.ui.commandNum = 0;
            }
        }

        if(code == KeyEvent.VK_ENTER) {
            if(gp.ui.commandNum == 0) { //if we press new game(which has 0 command num
                gp.gameState = gp.playState;
            }
            if(gp.ui.commandNum == 1) {
                gp.saveLoad.load();
                gp.gameState = gp.playState;
            }
            if(gp.ui.commandNum == 2) {
                System.exit(0);
            }
        }
    }

    /**
     * Handles key presses in the play state, for player moving.
     *
     * @param code the keycode of the pressed key
     */
    public void playState(int code){
        if ((code == KeyEvent.VK_W) || (code == KeyEvent.VK_UP)) {
            upPressed = true;
        }

        if ((code == KeyEvent.VK_S) || (code == KeyEvent.VK_DOWN)) {
            downPressed = true;
        }

        if ((code == KeyEvent.VK_A) || (code == KeyEvent.VK_LEFT)) {
            leftPressed = true;
        }

        if ((code == KeyEvent.VK_D) || (code == KeyEvent.VK_RIGHT)) {
            rightPressed = true;
        }

        if ((code == KeyEvent.VK_Q)) { //button for pausing the game -- Q
            gp.gameState = gp.pauseState; //pausing the game
        }
        if (code == KeyEvent.VK_R) {
            gp.gameState = gp.characterState;
        }

        if ((code == KeyEvent.VK_ENTER)) {
            enterPressed = true;
        }

        if ((code == KeyEvent.VK_F)) {
            shotKeyPressed = true;
        }

        if ((code == KeyEvent.VK_ESCAPE)) {
            gp.gameState = gp.optionsState;
        }
    }

    public void pauseState(int code){
        if ((code == KeyEvent.VK_Q)) { //button for pausing the game -- Q
            gp.gameState = gp.playState; //"unpausing" the game
        }
    }

    public void dialogueState(int code){
        if (code == KeyEvent.VK_SPACE) {
            gp.gameState = gp.playState;
        }
    }

    /**
     * Handles key presses in the character state(inventory, character status, level, etc).
     *
     * @param code the keycode of the pressed key
     */
    public void characterState(int code){
        if (code == KeyEvent.VK_R) {
            gp.gameState = gp.playState;
        }
        if (code == KeyEvent.VK_W){ if(gp.ui.slotRow != 0) {gp.ui.slotRow--;}}
        if (code == KeyEvent.VK_A){ if(gp.ui.slotCol != 0) {gp.ui.slotCol--;}}
        if (code == KeyEvent.VK_S){ if(gp.ui.slotRow != 3){gp.ui.slotRow++;}}
        if (code == KeyEvent.VK_D){ if(gp.ui.slotCol != 4){gp.ui.slotCol++;}}
        if(code == KeyEvent.VK_ENTER) { gp.player.selectItem(); }
    }

    /**
     * Handles key presses in the option state.
     *
     * @param code the keycode of the pressed key
     */
    public void optionsState(int code){
        if(code == KeyEvent.VK_ESCAPE){
            gp.gameState = gp.playState;
        }

        if(code == KeyEvent.VK_ENTER){
            enterPressed = true;
        }

        int maxCommandNum = 0;

        switch (gp.ui.subState) {
            case 0: maxCommandNum = 2; break;
            case 2: maxCommandNum = 1; break;
        }
        if((code == KeyEvent.VK_W)){
            gp.ui.commandNum--;
            if(gp.ui.commandNum < 0){
                gp.ui.commandNum = maxCommandNum;
            }
        }
        if((code == KeyEvent.VK_S)){
            gp.ui.commandNum++;
            if(gp.ui.commandNum > maxCommandNum){
                gp.ui.commandNum = 0;
            }
        }
    }

    /**
     * Handles key presses in the game over state, we can choose retry or exit game.
     *
     * @param code the keycode of the pressed key
     */
    public void gameOverState(int code){
        if(code== KeyEvent.VK_W){
            gp.ui.commandNum--;
            if(gp.ui.commandNum < 0){
                gp.ui.commandNum = 1;
            }
        }
        if(code== KeyEvent.VK_S){
            gp.ui.commandNum++;
            if(gp.ui.commandNum > 1){
                gp.ui.commandNum = 0;
            }
        }
        if(code== KeyEvent.VK_ENTER){
            if(gp.ui.commandNum == 0){
                gp.gameState = gp.playState;
                gp.resetGame(false);
            }
            else if(gp.ui.commandNum == 1){
                gp.gameState = gp.titleState;
                gp.resetGame(true);
            }
        }
    }

    /**
     * Handles the event when a key is released. (not pressed)
     *
     * @param e The KeyEvent representing the key release event.
     */
    @Override
    public void keyReleased(KeyEvent e) { //key is FREE

        int code = e.getKeyCode();

        if ((code == KeyEvent.VK_W) || (code == KeyEvent.VK_UP)) {
            upPressed = false;
        }

        if ((code == KeyEvent.VK_S) || (code == KeyEvent.VK_DOWN)) {
            downPressed = false;
        }

        if ((code == KeyEvent.VK_A) || (code == KeyEvent.VK_LEFT)) {
            leftPressed = false;
        }

        if ((code == KeyEvent.VK_D) || (code == KeyEvent.VK_RIGHT)) {
            rightPressed = false;
        }

        if ((code == KeyEvent.VK_F)) {
            shotKeyPressed = false;
        }

    }
}
