package org.example;

import org.example.data.SaveLoad;
import org.example.entity.Entity;
import org.example.entity.Player;
import org.example.tile.TileManager;
import org.example.tiles_interactive.InteractiveTile;

import javax.swing.JPanel;
import java.awt.*;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;



public class GamePanel extends JPanel implements Runnable {

    //SCREEN SETTINGS //TILE //PRIVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATE
    private final int firstTileSize = 16; // 16*16 tile, standart for characters and things
    private final int scale = 3; //number for multiplying *firstTileSize*
    public final int tileSize = firstTileSize * scale; //48*48 tile

    //WORLD SETTINGS
    public final int maxWorldCol = 50; //maximum of colomns in world map array
    public final int maxWorldRow = 50;

    //FPS
    int FPS = 60;

    //SCREEN
    private final int maxScreenColomns = 20; //kolik bude sloupcu
    private final int maxScreenRows = 12; //kolik bude radku

    public final int screenWidth = tileSize * maxScreenColomns; //960 pixels // 48*20
    public final int screenHeight = tileSize * maxScreenRows; //576 pixels ///////////////////////////


    //FOR SAVING AND LOADING
    SaveLoad saveLoad = new SaveLoad(this);

    //TILE TEXURES
    TileManager tileM = new TileManager(this);

    //KEYBOARD
    public Keyboard keyH = new Keyboard(this);

    //**!!!!THREAD THAT KEEPS PROGRAM RUNNING!!!!**//
    Thread gameThread;

    //COLLISIONCHECKER INSTANCE
    public CollisionChecker cChecker = new CollisionChecker(this); //passing gp

    //PLAYER CLASS INSTANCE
    public Player player = new Player(this, keyH);

    //OBJECT
    public Entity obj[] = new Entity[20]; //10 objects can be shown at the same time

    //MONSTER ARRAY
    public Entity monster[] = new Entity[20]; //we can display 20 monsters at the same time

    //NPC
    public Entity npc[] = new Entity[10]; //creating npc array

    //INTERACTIVE TILE
    public InteractiveTile iTile[] = new InteractiveTile[50];

    //RENDERING ORDER
    ArrayList<Entity> entityList = new ArrayList<>(); //putting all the entities in list, sort order of the array
    //we draw entities in order of their worldY value (the fewer, the earlier)

    //FOR MANA ATTACK
    public ArrayList<Entity> projectileList = new ArrayList<>();

    //INSTANCE ASSET SETTER
    public AssetSetter aSetter = new AssetSetter(this);

    //INSTANCE UI
    public UI ui = new UI(this);

    //INSTANCE EVENT
    public EventHandler eHandler = new EventHandler(this);

    //GAME STATE
    public int gameState;
    protected final int titleState = 0; //menu, title screen
    protected final int playState = 1; //int for state
    protected final int pauseState = 2; //int for pausing
    public final int dialogueState = 3; //for dialogues
    protected final int characterState = 4;
    protected final int optionsState = 5;
    public final int gameOverState = 6;

    public GamePanel() {
        this.setPreferredSize(new Dimension(screenWidth, screenHeight)); //set the size of JPanel class
        Color myColor = new Color(100, 150, 200);
        this.setBackground(Color.LIGHT_GRAY); //set the color of background
        this.setDoubleBuffered(true);
        this.addKeyListener(keyH);
        this.setFocusable(true); //???GamePanel can be 'focused' to receive key input
    }

    public void setupGame() { //method fot setting object, game state
        aSetter.setObject();
        aSetter.setNPC();
        aSetter.setMonster();
        aSetter.setInteractiveTile();
        //playMusic(0);
        gameState = titleState; // gameState = 1
    }

    public void resetGame(boolean restart){
        player.setDefaultPositions();
        player.restoreStatus();
        aSetter.setNPC();
        aSetter.setMonster();

        if(restart){
        player.setDefaultValues();
        aSetter.setObject();
        aSetter.setInteractiveTile();
        }
    }

    public void startGameThread() {

        gameThread = new Thread(this); //passing GamePanel
        gameThread.start(); //run method is called automatically
    }

    @Override
    /**
     * Starts the game thread, which keeps the program running.
     */
    public void run() {

        double drawInterval = 1000000000/FPS; //we draw the screen every 0.016 seconds ** 16 time per second
        double nextDrawTime = System.nanoTime() + drawInterval; //current time + drawInterval
        long timer = 0; //for FPS
        int drawCount = 0; //for FPS
        long currentTime; //for FPS
        long lastTime = System.nanoTime();


        /* GAME LOOP */
        while (gameThread != null) {


            currentTime = System.nanoTime();

            // 1 UPDATE: update information such as character
            update(); //call update() method
            // 2 DRAW: draw the screen with the updated information
            repaint(); //call paintComponent() method
            try {
                double remainingTime = nextDrawTime - System.nanoTime();
                remainingTime = remainingTime/1000000; //from nanosec to millisec

                if (remainingTime < 0) {
                    remainingTime = 0;
                }

                Thread.sleep((long) remainingTime); //stops game loop
                nextDrawTime += drawInterval;
                timer += (currentTime - lastTime); //for FPS
                drawCount++; //for FPS

            } catch (InterruptedException e) {
                LoggingManager.getLogger().severe("Game loop interrupted: " + e.getMessage());
            }
        }

    }

    /**
     * Updates the game state, including player, NPCs, monsters, and projectiles.
     * This method is responsible for updating the screen.
     */
    public void update() { //updating screen


        if (gameState == playState) { //game is continuing
            player.update(); //update player

            for (int i = 0; i < npc.length; i++){ //update npc
                if(npc[i] != null) {
                    npc[i].update();
                }
            }
            for (int i = 0; i < monster.length; i++){ //update monsters
                if(monster[i] != null) {
                    if(monster[i].alive && !monster[i].dying) { //check the monster is alive or not
                        monster[i].update();
                    }
                    if(!monster[i].alive) { //check the monster is alive or not
                        monster[i].checkDrop();
                        monster[i] = null;
                    }
                }
            }
            for (int i = 0; i < projectileList.size(); i++){ //update projectiles
                if(projectileList.get(i) != null) {
                    if(projectileList.get(i).alive) { //check the projectile is alive or not
                        projectileList.get(i).update();
                    }
                    if(!projectileList.get(i).alive) { //check the projectile is alive or not
                        projectileList.remove(i);
                    }
                }
            }
            for (int i = 0; i < iTile.length; i++){
                if(iTile[i] != null){
                    iTile[i].update();
                }
            }
        }
        if(gameState == pauseState) { //for pausing game
            //don't update player information, etc
                }
        }


        /**
         * Renders the game screen by drawing the tiles, entities, and UI elements.
         * @param g the Graphics object to draw on
         */
        public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g; //change Graphics to Graphics2D

        //TITLE SCREEN WILL BE HERE
        //TITLE SCREEN
        if(gameState == titleState) {
            ui.draw(g2);
        }
        //OTHERS
        else {
            //TILE
            tileM.draw(g2); //draw tile

            //interactive tiles
            for(int i = 0; i < iTile.length; i++) {
                if(iTile[i] != null){
                    iTile[i].draw(g2);
                }
            }
            //ADD ENTITIES TO THE LIST
            entityList.add(player); //adding player to the list

            for (int i = 0; i < npc.length; i++){ //npcs
                if(npc[i] != null){
                    entityList.add(npc[i]);
                }
            }

            for (int j = 0; j < obj.length; j++){ //objects
                if(obj[j] != null) {
                    entityList.add(obj[j]);
                }
            }

            for (int j = 0; j < monster.length; j++){ //monsters
                if(monster[j] != null) {
                    entityList.add(monster[j]);
                }
            }

            for (int j = 0; j < projectileList.size(); j++){ //projectiles
                if(projectileList.get(j) != null) {
                    entityList.add(projectileList.get(j));
                }
            }

            //SORTING THE ORDER IN ARRAYLIST
            Collections.sort(entityList, new Comparator<Entity>() {
                @Override
                public int compare(Entity e1, Entity e2) { //we want to compare entities by their worldY
                    return Integer.compare(e1.worldY, e2.worldY);
                }
            });

            //DRAW ENTITIES IN SORTED ARRAYLIST ONE BY ONE
            for (int i = 0; i < entityList.size(); i++) {
                entityList.get(i).draw(g2);
            }

            //EMPTY ENTITY LIST
            entityList.clear();

            //UI
            ui.draw(g2); //we call the ui method after player because ui should be on the last layer
        }

        g2.dispose();
    }
}
