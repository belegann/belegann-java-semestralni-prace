package org.example;

import java.awt.*;

public class EventHandler {

    GamePanel gp;
    EventRect eventRect[][];
    int previousEventX, previousEventY; //for not repeating event in tile
    boolean canTouchEvent = true; //for not repeating event in tile

    public EventHandler(GamePanel gp) {
        this.gp = gp;

        eventRect= new EventRect[gp.maxWorldCol][gp.maxWorldRow];
        //setting solidArea on this array
        int col = 0;
        int row = 0;

        while(col < gp.maxWorldCol && row < gp.maxWorldRow) {
            eventRect[col][row] = new EventRect();
            eventRect[col][row].x = 23;
            eventRect[col][row].y = 23;
            eventRect[col][row].width = 6;
            eventRect[col][row].height = 6;
            eventRect[col][row].eventRectDefaultX = eventRect[col][row].x;
            eventRect[col][row].eventRectDefaultY = eventRect[col][row].y;
            col++;
            if(col == gp.maxWorldCol) {
                col = 0;
                row++;
            }
        }

    }

    /**
     * Starting event.
     */
    public void checkEvent() { //for checking event

        //Check if player is more than 1 tile away from the last event
        int xDistance = Math.abs(gp.player.worldX - previousEventX);
        int yDistance = Math.abs(gp.player.worldY - previousEventY);
        int distance = Math.max(xDistance, yDistance);

        if (distance > gp.tileSize) {
            canTouchEvent = true;
        }

        if (canTouchEvent) {
            for (int i = 31; i <= 49;  i++) {
                    if (hit(i, 1, "any") == true) {
                        //LAVA EVENT HAPPENS
                        Lava(i , 1, gp.dialogueState);
                    }
            }

            for (int i = 6; i <= 11;  i++) {
                for (int j = 6; j <= 7 ;  j++) {

                    if (hit(i, j, "any") == true) {
                        //MILK EVENT HAPPENS
                        Milk(i , j, gp.dialogueState);
                    }
                }
            }

        }

    }

    /**
     * Checks event collision at the specified tile coordinates.
     *
     * @param col           the column index of the event tile
     * @param row           the row index of the event tile
     * @param regDirection  the direction for collision checking
     * @return true if event collision occurs, false otherwise
     */
    public boolean hit(int col, int row, String regDirection) { //checks event collision
        boolean hit = false;

        //getting player's current solidArea positions
        gp.player.solidArea.x = gp.player.worldX + gp.player.solidArea.x;
        gp.player.solidArea.y = gp.player.worldY + gp.player.solidArea.y;
        //getting event rectangles solidArea positions
        eventRect[col][row].x = col * gp.tileSize + eventRect[col][row].x;
        eventRect[col][row].y = row * gp.tileSize + eventRect[col][row].y;

        //check if player (player's solidArea) colliding this event rectangle (eventRect's solidArea)

        if(gp.player.solidArea.intersects(eventRect[col][row]) && eventRect[col][row].eventDone == false) {
            if(gp.player.direction.contentEquals(regDirection) || regDirection.contentEquals("any")) {
                 hit = true; //if collision is happening, hit is TRUE

                previousEventX = gp.player.worldX;
                previousEventY = gp.player.worldY;
            }
        }

        //after checking the collision reset the solidArea x and y
        gp.player.solidArea.x = gp.player.solidAreaDefaultX;
        gp.player.solidArea.y = gp.player.solidAreaDefaultY;
        eventRect[col][row].x = eventRect[col][row].eventRectDefaultX;
        eventRect[col][row].y = eventRect[col][row].eventRectDefaultY;

        return hit;
    }

    public void Lava(int col, int row, int gameState) { //LAVA EVENT (damage)

        gp.gameState = gameState;
        gp.ui.currentDialogue = "You are in LAVA!!!";
        gp.player.life -= 1;
        //eventRect[col][row].eventDone = true;
        canTouchEvent = false;
    }

    public void Milk(int col, int row, int gameState) { //MILK EVENT (healing)

        gp.gameState = gameState;
        gp.ui.currentDialogue = "You are in MILK!\n"+
        "The progress has been saved.";
        gp.player.life = gp.player.maxLife;
        gp.player.mana = gp.player.maxMana;
        //gp.player.life = gp.player.maxLife;
        //eventRect[col][row].eventDone = true;
        canTouchEvent = false;
        gp.saveLoad.save();
    }
}
