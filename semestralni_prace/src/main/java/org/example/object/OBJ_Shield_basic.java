package org.example.object;


import org.example.GamePanel;
import org.example.entity.Entity;

public class OBJ_Shield_basic extends Entity {
    public OBJ_Shield_basic(GamePanel gp) {
        super(gp);

        type = type_shield;
        name = "Shield";
        down1 = setup("res/objects/testshield", gp.tileSize, gp.tileSize);
        defenseValue = 1;
        description = "[" + name + "]\nAn gold shield, given for\nall Nimbus cats to protect\nyourself.";
    }
}
