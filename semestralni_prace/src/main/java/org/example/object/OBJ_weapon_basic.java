package org.example.object;


import org.example.GamePanel;
import org.example.entity.Entity;

public class OBJ_weapon_basic extends Entity {
    public OBJ_weapon_basic(GamePanel gp) {
        super(gp);


        type = type_trident;
        name = "Trident";
        down1 = setup("res/objects/trident_icon",gp.tileSize, gp.tileSize);
        attackValue = 1;
        attackArea.width = 36;
        attackArea.height = 36;
        description = "[" + name + "]\nAn gold trident, given for\nall Nimbus cats to protect\nyourself.";

    }
}
