package org.example.object;


import org.example.GamePanel;
import org.example.entity.Entity;

public class OBJ_Coin extends Entity {
    GamePanel gp;
    public OBJ_Coin(GamePanel gp) {
        super(gp);
        this.gp = gp;

        type = type_pickupOnly;
        name = "Silver Coin";
        value = 1;
        down1 = setup("res/objects/silvercointest", gp.tileSize, gp.tileSize);
    }

    public boolean use(Entity entity) { //using potion

        gp.ui.addMessage("Coin +" + value);
        gp.player.coin += value;
        return true;
    }
}
