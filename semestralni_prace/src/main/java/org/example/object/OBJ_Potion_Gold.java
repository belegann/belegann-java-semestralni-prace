package org.example.object;


import org.example.GamePanel;
import org.example.entity.Entity;

public class OBJ_Potion_Gold extends Entity {

    GamePanel gp;
    public OBJ_Potion_Gold(GamePanel gp) {
        super(gp);

        this.gp = gp;

        type = type_consumable;
        name = "Gold Potion";
        value = 5;
        down1 = setup("res/objects/potion", gp.tileSize, gp.tileSize);
        description = "[" + name + "]\nHeals your life by " + value + ".";
        stackable = true;

    }

    public boolean use(Entity entity) { //using potion

        gp.gameState = gp.dialogueState;
        gp.ui.currentDialogue = "You drink the " + name + "!\n" +
        "Your life has been recovered by " + value + ".";

        entity.life += value;
        return true;
    }
}
