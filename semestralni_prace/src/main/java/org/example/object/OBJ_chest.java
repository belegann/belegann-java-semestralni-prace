package org.example.object;
import org.example.GamePanel;
import org.example.entity.Entity;


public class OBJ_chest extends Entity {

    GamePanel gp;
    public OBJ_chest(GamePanel gp) {

        super(gp);
        this.gp = gp;

        type = type_obstacle;
        name = "Chest";
        down1 = setup("res/objects/chest", gp.tileSize, gp.tileSize);
        collision = true; //making the chest solid

        solidArea.x = 0;
        solidArea.y = 16;
        solidArea.width = 48;
        solidArea.height = 32;
        solidAreaDefaultX = solidArea.x;
        solidAreaDefaultY = solidArea.y;
    }

    public void interact() {
        gp.gameState = gp.dialogueState;
        gp.ui.currentDialogue = "You need a key to open this";
    }
}
