package org.example.object;


import org.example.GamePanel;
import org.example.entity.Entity;

public class OBJ_Health extends Entity {

    GamePanel gp;

    public OBJ_Health(GamePanel gp) {

        super(gp);
        this.gp = gp;
        type = type_pickupOnly;
        name = "Health";
        value = 2;
        down1 = setup("res/objects/fullhealth", gp.tileSize, gp.tileSize);
        image = setup("res/objects/fullhealth", gp.tileSize, gp.tileSize);
        image2 = setup("res/objects/halfhealth", gp.tileSize, gp.tileSize);
        image3 = setup("res/objects/emptyhealth", gp.tileSize, gp.tileSize);
    }

    public boolean use (Entity entity){
        gp.ui.addMessage("Life +" + value);
        entity.life += value;
        return true;
    }
}
