package org.example.object;
import org.example.GamePanel;
import org.example.entity.Entity;


public class OBJ_boots extends Entity {

    GamePanel gp;

    public OBJ_boots(GamePanel gp) {

        super(gp);
        this.gp = gp;

        type = type_pickupOnly;
        name = "Boots";
        //value = 1;
        down1 = setup("res/objects/silvercointest", gp.tileSize, gp.tileSize);
    }
}
