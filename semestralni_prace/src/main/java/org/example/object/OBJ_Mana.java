package org.example.object;


import org.example.GamePanel;
import org.example.entity.Entity;

public class OBJ_Mana extends Entity {

    GamePanel gp;
    public OBJ_Mana(GamePanel gp) {
        super(gp);
        this.gp = gp;

        type = type_pickupOnly;
        name = "Mana";
        value = 1;
        down1 = setup("res/objects/manafull", gp.tileSize, gp.tileSize);
        image = setup("res/objects/manafull", gp.tileSize, gp.tileSize);
        image2 = setup("res/objects/manaempty", gp.tileSize, gp.tileSize);
    }
    public boolean use (Entity entity){
        gp.ui.addMessage("Mana +" + value);
        entity.mana += value;
        return true;
    }
}
