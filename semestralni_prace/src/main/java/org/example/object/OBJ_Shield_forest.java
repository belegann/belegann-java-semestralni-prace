package org.example.object;


import org.example.GamePanel;
import org.example.entity.Entity;

public class OBJ_Shield_forest extends Entity {
    public OBJ_Shield_forest(GamePanel gp) {
        super(gp);

        type = type_shield;
        name = "Forest Shield";
        down1 = setup("res/objects/forestshield", gp.tileSize, gp.tileSize);
        defenseValue = 2;
        description = "[" + name + "]\nA forest shield!\nGood for killing Forest Amoguses";
    }
}
