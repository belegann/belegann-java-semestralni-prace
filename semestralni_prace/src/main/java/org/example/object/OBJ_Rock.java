package org.example.object;


import org.example.GamePanel;
import org.example.entity.Entity;
import org.example.entity.Projectile;

public class OBJ_Rock extends Projectile {

    GamePanel gp;
    public OBJ_Rock(GamePanel gp) {
        super(gp);
        this.gp = gp;

        name = "Rock";
        speed = 2; //how fast can fly
        maxLife = 80;
        life = maxLife;
        attack = 2;
        useCost = 1; //we use 1 mana to cast the spell
        alive = false;
        getImage();
    }

    public void getImage() {
        up1 = setup("res/projectile/rocktest", gp.tileSize, gp.tileSize);
        up2 = setup("res/projectile/rocktest", gp.tileSize, gp.tileSize);
        down1 = setup("res/projectile/rocktest", gp.tileSize, gp.tileSize);
        down2 = setup("res/projectile/rocktest", gp.tileSize, gp.tileSize);
        left1 = setup("res/projectile/rocktest", gp.tileSize, gp.tileSize);
        left2 = setup("res/projectile/rocktest", gp.tileSize, gp.tileSize);
        right1 = setup("res/projectile/rocktest", gp.tileSize, gp.tileSize);
        right2 = setup("res/projectile/rocktest", gp.tileSize, gp.tileSize);
    }

    public boolean haveResource(Entity user){ //using mana

        boolean haveResource = false;
        if(user.ammo >= useCost) {
            haveResource = true;
        }
        return haveResource;
    }

    public void subtractResource(Entity user){
        user.ammo -= useCost;
    }
}
