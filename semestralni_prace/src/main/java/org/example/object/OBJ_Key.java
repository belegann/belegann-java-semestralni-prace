package org.example.object;


import org.example.GamePanel;
import org.example.entity.Entity;


//subclass of SuperObject class
public class OBJ_Key extends Entity {

    GamePanel gp;

    public OBJ_Key(GamePanel gp) {
        super(gp);
        this.gp = gp;

        type = type_consumable;
        name = "Fish";
        down1 = setup("res/objects/fish", gp.tileSize, gp.tileSize);
        description = "[" + name + "]\nThis is food, you think?\nNo! It's a golden fish key!";
        stackable = true;

    }

    /*public boolean use(Entity entity){
        gp.gameState = gp.dialogueState;
         int objIndex = getDetected(entity, gp.obj, "Door");

         if(objIndex != 999){
             gp.ui.currentDialogue = "You use the " + name + "and open the door";
             gp.obj[objIndex] = null;
             return true;
         }
         else{
             gp.ui.currentDialogue = "What are you doing?";
             return false;
         }
    } */
}
