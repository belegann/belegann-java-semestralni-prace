package org.example.object;


import org.example.GamePanel;
import org.example.entity.Entity;

public class OBJ_Axe extends Entity {
    public OBJ_Axe(GamePanel gp) {
        super(gp);

        type = type_axe;
        name = "Forest Axe";
        down1 = setup("res/objects/forestaxe", gp.tileSize, gp.tileSize);
        attackValue = 2;
        attackArea.width = 30;
        attackArea.height = 30;
        description = "[" + name + "]\nGood forest Axe, made by Pepe!";
    }
}
