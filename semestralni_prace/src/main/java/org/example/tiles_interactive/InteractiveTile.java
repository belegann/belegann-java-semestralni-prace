package org.example.tiles_interactive;

import org.example.GamePanel;
import org.example.entity.Entity;


public class InteractiveTile extends Entity {

    GamePanel gp;
    public boolean destructible = false;
    public InteractiveTile(GamePanel gp, int col, int row) {
        super(gp);
        this.gp = gp;
    }

    public boolean isCorrectItem(Entity entity){ //checking if we have right weapon to cut trees
        boolean isCorrectItem = false;
        return isCorrectItem;
    }
    public InteractiveTile getDestroyedRForm(){ //stump(trunk) of the tree
        InteractiveTile tile = null;
        return tile;
    }
    public void update(){
        if(invincible) {
            invincibleCounter++;
            if (invincibleCounter > 20) { //0.6 second, 40 frames
                invincible = false;
                invincibleCounter = 0; //reset the counter
            }
        }
    }
}
