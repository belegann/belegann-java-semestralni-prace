package org.example.tiles_interactive;

import org.example.GamePanel;
import org.example.entity.Entity;

public class IT_DryTree extends InteractiveTile{

    GamePanel gp;
    public IT_DryTree(GamePanel gp, int col, int row) {
        super(gp, col, row);
        this.gp = gp;

        this.worldX = gp.tileSize * col;
        this.worldY = gp.tileSize * row;

        down1 = setup("res/tile_interactive/drytree", gp.tileSize, gp.tileSize);
        destructible = true;
        life = 3;
    }

    public boolean isCorrectItem(Entity entity){ //checking if we have right weapon to cut trees
        boolean isCorrectItem = false;
        if(entity.currentWeapon.type == type_axe){
            isCorrectItem = true;
        }
        return isCorrectItem;
    }

    public InteractiveTile getDestroyedRForm(){ //stump(trunk) of the tree
        return new IT_Trunk(gp, worldX/gp.tileSize, worldY/gp.tileSize);
    }

}
