package org.example;

import org.example.entity.Entity;
import org.example.object.*;


import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;


public class UI { //item icons, text messages
    GamePanel gp;
    Graphics2D g2;
    Font myfont;
    //BufferedImage keyImage;
    BufferedImage fullhealth, halfhealth, emptyhealth; //for health
    BufferedImage manafull, manaempty;
    public boolean messageOn = false;

    //*********UPDATES FOR MESSAGES************//
    //public String message = "";
    //int messageCounter = 0; //for dissapearing message after getting a key(fish)
    //NEW:
    ArrayList<String> message = new ArrayList<>();
    ArrayList<Integer> messageCounter = new ArrayList<>();

    //*******************************//
    public String currentDialogue = "";
    public int commandNum = 0; //for menu/title screen to load that we want
    public int slotCol = 0; //for inventory
    public int slotRow = 0; //for inventory
    int subState = 0;
    public UI(GamePanel gp) {
        this.gp = gp;

        //CREATE HUD OBJECT
        Entity heart = new OBJ_Health(gp);
        fullhealth = heart.image;
        halfhealth = heart.image2;
        emptyhealth = heart.image3;
        Entity crystal = new OBJ_Mana(gp);
        manafull = crystal.image;
        manaempty = crystal.image2;
        loadFont();

    }

    public void loadFont(){
        try {
            InputStream is = new BufferedInputStream(Files.newInputStream(Paths.get("res/font/VCR_OSD_MONO_1.001.ttf")));
            myfont = Font.createFont(Font.TRUETYPE_FONT, is);
        } catch (FontFormatException e) {
            LoggingManager.getLogger().severe("Font format exception " + e.getMessage());
        } catch (IOException e) {
            LoggingManager.getLogger().severe("IO exception while loading font: " + e.getMessage());
        }
    }

    public void addMessage(String text) { //showing notification message

        message.add(text);
        messageCounter.add(0); //default value of massageCounter
    }

    /**
     * Calling methods for different ui windows.
     */
    public void draw(Graphics2D g2) {
        /* UPDATING METHOD */
        this.g2 = g2;
        g2.setFont(myfont);
        g2.setColor(Color.white);
        //TITLE STATE
        if(gp.gameState == gp.titleState) {
            drawTitleScreen();
        }
        //PLAY STATE
        if (gp.gameState == gp.playState) {
            drawPlayerLife();
            drawMessage();
        }

        //PAUSE STATE
        if (gp.gameState == gp.pauseState) { //if game is pausing
            drawPlayerLife();
            drawPauseScreen();
        }
        //DIALOGUE STATE
        if (gp.gameState == gp.dialogueState) {
            drawPlayerLife();
            drawDialogueScreen(); //calling method for dialogues
            }

        //CHARACTER STATE
        if(gp.gameState == gp.characterState){
            drawCharacterScreen();
            drawInventory();
        }

        //OPTIONS STATE
        if(gp.gameState == gp.optionsState) {
            drawOptionsScreen();
        }

        //GAME OVER STATE
        if(gp.gameState == gp.gameOverState) {
            drawGameOverScreen();
        }
    }

    /**
     * Game over screen.
     */
    public void drawGameOverScreen(){ //game over screen
        g2.setColor(new Color(0, 0, 0, 150));
        g2.fillRect(0, 0, gp.screenWidth, gp.screenHeight);

        int x;
        int y;
        String text;
        g2.setFont(g2.getFont().deriveFont(Font.BOLD, 110f));

        text = "Game Over";
        // Shadow
        g2.setColor(Color.black);
        x = getXforCenteredText(text);
        y = gp.tileSize * 4;
        g2.drawString(text, x, y);
        //Main
        g2.setColor(Color.white);
        g2.drawString(text, x-4, y-4);

        //Retry
        g2.setFont(g2.getFont().deriveFont(50f));
        text = "Retry";
        x = getXforCenteredText(text);
        y+= gp.tileSize*4;
        g2.drawString(text, x, y);
        if(commandNum == 0) {
            g2.drawString(">", x-40, y);
        }


        //Back to the title screen
        text = "Quit";
        x = getXforCenteredText(text);
        y += 55;
        g2.drawString(text, x, y);
        if(commandNum == 1) {
            g2.drawString(">", x-40, y);
        }
    }

    /**
     * Draw Player Life.
     */
    public void drawPlayerLife() { //draw player life + mana
        //gp.player.life = 3;

        int x = gp.tileSize / 2; //x coordinates for life pics
        int y = gp.tileSize / 2; //y coordinates for life pics
        //handle these pics with while loop
        int i = 0;

        //DRAW EMPTY HEALTH (MAX LIFE)
        while(i < gp.player.maxLife/2) {
            g2.drawImage(emptyhealth, x, y, null);
            i++;
            x += gp.tileSize + 2;
        }

        //RESET
        x = gp.tileSize / 2;
        y = gp.tileSize / 2;
        i = 0;

        //DRAW CURRENT LIFE
        while (i < gp.player.life) {
            g2.drawImage(halfhealth, x, y, null);
            i++;
            if(i < gp.player.life) {
                g2.drawImage(fullhealth, x, y, null);
            }
            i++;
            x += gp.tileSize + 2;
        }

        //DRAW MAX MANA
        x = gp.tileSize/2 - 5;
        y = (int)(gp.tileSize * 1.5);
        i = 0;
        while(i < gp.player.maxMana) {
            g2.drawImage(manaempty, x, y, null);
            i++;
            x += 35;
        }

        //DRAW MANA
        x = gp.tileSize/2 - 5;
        y = (int)(gp.tileSize * 1.5);
        i = 0;
        while(i < gp.player.mana){
            g2.drawImage(manafull, x, y, null);
            i++;
            x += 35;
        }
    }

    /**
     * Writing pop-up messages.
     */
    public void drawMessage() {

        int messageX = gp.tileSize;
        int messageY = gp.tileSize * 4;

        g2.setFont(g2.getFont().deriveFont(Font.PLAIN, 32F));

        for (int i = 0; i < message.size();  i++){

            if(message.get(i) != null){

                g2.setColor(Color.black);
                g2.drawString(message.get(i), messageX+2, messageY+2);

                g2.setColor(Color.blue);
                g2.drawString(message.get(i), messageX, messageY);


                int counter = messageCounter.get(i) + 1;
                messageCounter.set(i, counter);
                messageY += 50;

                if (messageCounter.get(i) > 180) {
                    message.remove(i);
                    messageCounter.remove(i);
                }
            }
        }
    }

    /**
     * Drawing title screen.
     */
    public void drawTitleScreen() { //title screen, menu

        g2.setColor(new Color(0, 0, 0));
        g2.fillRect(0, 0, gp.screenWidth, gp.screenHeight);

        //TITLE NAME
        g2.setFont(g2.getFont().deriveFont(Font.PLAIN, 60F));
        String text = "NIMBUS CAT ADVENTURE";
        int x = getXforCenteredText(text); //coordinate x of title
        int y = gp.tileSize * 2; //coordinate y of title

        //SHADOW
        g2.setColor(Color.cyan);
        g2.drawString(text, x + 4, y + 4);

        //SHADOW 2
        g2.setColor(Color.red);
        g2.drawString(text, x - 2 , y - 2);

        //MAIN COLOR
        g2.setColor(Color.white); //color of title
        g2.drawString(text, x, y);

        //NIMBUS CAT IMAGE
        x = gp.screenWidth/2 - (gp.tileSize*2)/2;
        y += gp.tileSize * 2;
        g2.setColor(Color.cyan);
        g2.fillRect(x, y, gp.tileSize + 45, gp.tileSize + 45 );
        g2.drawImage(gp.player.down1, x, y, gp.tileSize * 2, gp.tileSize * 2, null);

        //MENU
        g2.setFont(g2.getFont().deriveFont(Font.PLAIN, 40F));
        text = "NEW GAME";
        x = getXforCenteredText(text);
        y += gp.tileSize * 4;
        g2.setColor(Color.white);
        g2.drawString(text, x, y);
        if(commandNum == 0) {
            g2.drawString(">", x-gp.tileSize, y);
        }

        g2.setFont(g2.getFont().deriveFont(Font.PLAIN, 40F));
        text = "LOAD GAME";
        x = getXforCenteredText(text);
        y += gp.tileSize;
        g2.drawString(text, x, y);
        if(commandNum == 1) {
            g2.drawString(">", x-gp.tileSize, y);
        }


        g2.setFont(g2.getFont().deriveFont(Font.PLAIN, 40F));
        text = "EXIT";
        x = getXforCenteredText(text);
        y += gp.tileSize;
        g2.drawString(text, x, y);
        if(commandNum == 2) {
            g2.drawString(">", x-gp.tileSize, y);
        }

    }

    public void drawPauseScreen() { //screen for pausing game

        g2.setFont(g2.getFont().deriveFont(Font.PLAIN, 80F)); //larger font
        String text = "PAUSED";
        int x = getXforCenteredText(text); //calling the method under
        int y = gp.screenHeight/2;
        g2.drawString(text, x, y); //write that game is paused

    }

    public void drawDialogueScreen() { //draw dialogue screen

        //WINDOW
        int x = gp.tileSize * 2;
        int y = gp.tileSize / 2;
        int width = gp.screenWidth - (gp.tileSize * 4);
        int height = gp.tileSize * 3;

        drawSubWindow(x, y, width, height);

        g2.setColor(Color.white);
        g2.setFont(g2.getFont().deriveFont(Font.PLAIN, 25F));
        x += gp.tileSize; //deciding there will the the text in window
        y += gp.tileSize;

        for(String line : currentDialogue.split("\n")){ //for splitting long lines
            g2.drawString(line, x, y);
            y += 40; //the second line will be displayed lower
        }
        //g2.drawString(currentDialogue, x, y);
    }

    /**
     * Drawing character actual status screen.
     */
    public void drawCharacterScreen(){ //method that shows window with character status

        // CREATE A FRAME
        final int frameX = gp.tileSize * 2;
        final int frameY = gp.tileSize;
        final int frameWidth = gp.tileSize * 5;
        final int frameHeight = gp.tileSize * 10;
        drawSubWindow(frameX, frameY, frameWidth, frameHeight);

        //TEXT
        g2.setColor(Color.white);
        g2.setFont(g2.getFont().deriveFont(24F));

        int textX = frameX + 20;
        int textY = frameY + gp.tileSize;
        final int lineHeight = 32; //same as the front size

        //NAMES
        g2.drawString("Level", textX, textY);
        textY += lineHeight;
        g2.drawString("Life", textX, textY);
        textY += lineHeight;
        g2.drawString("Mana", textX, textY);
        textY += lineHeight;
        g2.drawString("Strength", textX, textY);
        textY += lineHeight;
        g2.drawString("Dexterity", textX, textY);
        textY += lineHeight;
        g2.drawString("Attack", textX, textY);
        textY += lineHeight;
        g2.drawString("Defense", textX, textY);
        textY += lineHeight;
        g2.drawString("Exp", textX, textY);
        textY += lineHeight;
        g2.drawString("Exp need", textX, textY);
        textY += lineHeight;
        g2.drawString("Coin", textX, textY);
        textY += lineHeight + 10;
        g2.drawString("Weapon", textX, textY);
        textY += lineHeight + 15;
        g2.drawString("Shield", textX, textY);
        textY += lineHeight;

        //VALUES
        int tailX = (frameX + frameWidth) - 30;
        //Reset textY
        textY = frameY + gp.tileSize;
        String value;

        value = String.valueOf(gp.player.level);
        textX = getXforAlignToRightText(value, tailX);
        g2.drawString(value, textX, textY);
        textY += lineHeight;

        value = String.valueOf(gp.player.life + "/" + gp.player.maxLife);
        textX = getXforAlignToRightText(value, tailX);
        g2.drawString(value, textX, textY);
        textY += lineHeight;

        value = String.valueOf(gp.player.mana + "/" + gp.player.maxMana);
        textX = getXforAlignToRightText(value, tailX);
        g2.drawString(value, textX, textY);
        textY += lineHeight;

        value = String.valueOf(gp.player.strength);
        textX = getXforAlignToRightText(value, tailX);
        g2.drawString(value, textX, textY);
        textY += lineHeight;

        value = String.valueOf(gp.player.dexterity);
        textX = getXforAlignToRightText(value, tailX);
        g2.drawString(value, textX, textY);
        textY += lineHeight;

        value = String.valueOf(gp.player.attack);
        textX = getXforAlignToRightText(value, tailX);
        g2.drawString(value, textX, textY);
        textY += lineHeight;

        value = String.valueOf(gp.player.defense);
        textX = getXforAlignToRightText(value, tailX);
        g2.drawString(value, textX, textY);
        textY += lineHeight;

        value = String.valueOf(gp.player.exp);
        textX = getXforAlignToRightText(value, tailX);
        g2.drawString(value, textX, textY);
        textY += lineHeight;

        value = String.valueOf(gp.player.nextLevelExp);
        textX = getXforAlignToRightText(value, tailX);
        g2.drawString(value, textX, textY);
        textY += lineHeight;

        value = String.valueOf(gp.player.coin);
        textX = getXforAlignToRightText(value, tailX);
        g2.drawString(value, textX, textY);
        textY += lineHeight;

        g2.drawImage(gp.player.currentWeapon.down1, tailX - gp.tileSize + 5, textY-14, null);
        textY += gp.tileSize + 2;
        g2.drawImage(gp.player.currentShield.down1, tailX - gp.tileSize + 5, textY-14, null);

    }

    /**
     * Drawing inventory.
     */
    public void drawInventory(){

        //frame
        int frameX = gp.tileSize * 9;
        int frameY = gp.tileSize;
        int frameWidth = gp.tileSize * 6;
        int frameHeight = gp.tileSize * 5;
        drawSubWindow(frameX, frameY, frameWidth, frameHeight);

        //slot
        final int slotXstart = frameX + 20; //upper left corner of inventory
        final int slotYstart = frameY + 20;

        int slotX = slotXstart; //values for another slots, can be changed
        int slotY = slotYstart;
        int slotSize = gp.tileSize+3;

        //draw player's items
        for (int i = 0; i < gp.player.inventory.size(); i++) {

            //equip cursor
            if(gp.player.inventory.get(i) == gp.player.currentWeapon ||
            gp.player.inventory.get(i) == gp.player.currentShield){
                g2.setColor(new Color(240, 190, 90, 80));
                g2.fillRoundRect(slotX, slotY, gp.tileSize, gp.tileSize, 10, 10);
            }

            g2.drawImage(gp.player.inventory.get(i).down1, slotX, slotY, null);

            //display the amount
            if(gp.player.inventory.get(i).amount > 1){

                g2.setFont(g2.getFont().deriveFont(32f));
                g2.setColor(Color.white);
                int amountX;
                int amountY;

                String s = "" + gp.player.inventory.get(i).amount;
                amountX = getXforAlignToRightText(s, slotX + 44);
                amountY = slotY + gp.tileSize;

                //shadow
                g2.setColor(new Color(60, 60, 60));
                g2.drawString(s, amountX, amountY);
                //number
                g2.setColor(Color.white);
                g2.drawString(s, amountX-3, amountY-3);

            }

            slotX += slotSize;

            if(i == 4 || i == 9 || i == 14) {
                slotX = slotXstart;
                slotY += slotSize;
            }

        }

        //cursor for selecting item
        int cursorX = slotXstart + (slotSize * slotCol);
        int cursorY = slotYstart + (slotSize * slotRow);
        int cursorWidth = gp.tileSize;
        int cursorHeight = gp.tileSize;

        //draw cursor
        g2.setColor(Color.white);
        g2.setStroke(new BasicStroke(3));
        g2.drawRoundRect(cursorX, cursorY, cursorWidth, cursorHeight, 10, 10);

        //description frame
        int dFrameX = frameX;
        int dFrameY = frameY + frameHeight + 5;
        int dFrameWidth = frameWidth;
        int dFrameHeight = gp.tileSize * 4;


        //description text
        int textX = dFrameX + 20;
        int textY = dFrameY + gp.tileSize;
        g2.setFont(g2.getFont().deriveFont(15F));
        g2.setColor(Color.white);

        int itemIndex = getItemIndexOnSlot();

        if(itemIndex < gp.player.inventory.size()) {

            drawSubWindow(dFrameX, dFrameY, dFrameWidth, dFrameHeight);
            g2.setColor(Color.white);

            for(String line: gp.player.inventory.get(itemIndex).description.split("\n")) {
                g2.drawString(line, textX, textY);
                textY += 32;
            }
        }
    }

    /**
     * Drawing options screen.
     */
    public void drawOptionsScreen(){

        g2.setColor(Color.white);
        g2.setFont(g2.getFont().deriveFont(32F));

        //SUBWINDOW
        int frameX = gp.tileSize*6;
        int frameY = gp.tileSize;
        int frameWidth = gp.tileSize*8;
        int frameHeight = gp.tileSize*10;
        drawSubWindow(frameX, frameY, frameWidth, frameHeight);

        switch(subState){
            case 0: options_top(frameX, frameY); break;
            case 1: options_control(frameX, frameY); break;
            case 2: options_endGameConfirmation(frameX, frameY); break;
        }
        gp.keyH.enterPressed = false;
    }

    /**
     * Renders the top section of the options window.
     *
     * @param frameX the x-coordinate of the options window frame for writing text
     * @param frameY the y-coordinate of the options window frame for writing text
     */
    public void options_top(int frameX, int frameY){ //top of options window

        int textX;
        int textY;

        //TITLE
        String text = "Options";
        g2.setColor(Color.white);
        textX = getXforCenteredText(text);
        textY = frameY + gp.tileSize;
        g2.drawString(text, textX, textY);

        //CONTROL
        textX = frameX + gp.tileSize;
        textY += gp.tileSize * 2;
        g2.drawString("Control", textX, textY);
        if(commandNum == 0){
            g2.drawString(">", textX-25, textY);
            if(gp.keyH.enterPressed == true){
                subState = 1;
                commandNum = 0;
            }
        }
        //END GAME
        textY += gp.tileSize;
        g2.drawString("End Game", textX, textY);
        if(commandNum == 1){
            g2.drawString(">", textX-25, textY);
            if(gp.keyH.enterPressed == true){
                subState = 2;
                commandNum = 0;
            }
        }

        //BACK
        textY += gp.tileSize * 2;
        g2.drawString("Back", textX, textY);
        if(commandNum == 2){
            g2.drawString(">", textX-25, textY);
            if(gp.keyH.enterPressed == true){
                gp.gameState = gp.playState;
                commandNum = 0;
            }
        }
    }

    /**
     * Renders the CONTROL section of the options window.
     *
     * @param frameX the x-coordinate of the options window frame for writing text
     * @param frameY the y-coordinate of the options window frame for writing text
     */
    public void options_control(int frameX, int frameY){

        int textX;
        int textY;

        g2.setColor(Color.white);
        g2.setFont(g2.getFont().deriveFont(28F));

        //TITLE
        String text = "Control";
        textX = getXforCenteredText(text);
        textY = frameY + gp.tileSize;
        g2.drawString(text, textX, textY);

        textX = frameX + gp.tileSize - 31;
        textY += gp.tileSize;
        g2.drawString("Move", textX, textY); textY += gp.tileSize;
        g2.drawString("Confirm/attack", textX, textY); textY += gp.tileSize;
        g2.drawString("Shoot/Cast", textX, textY); textY += gp.tileSize;
        g2.drawString("Character Screen", textX, textY); textY += gp.tileSize;
        g2.drawString("Pause", textX, textY); textY += gp.tileSize;
        g2.drawString("Options", textX, textY); textY += gp.tileSize;

        textX = frameX + gp.tileSize*6;
        textY = frameY + gp.tileSize*2;
        g2.drawString("WASD", textX, textY); textY += gp.tileSize;
        g2.drawString("ENTER", textX, textY); textY += gp.tileSize;
        g2.drawString("F", textX, textY); textY += gp.tileSize;
        g2.drawString("R", textX, textY); textY += gp.tileSize;
        g2.drawString("Q", textX, textY); textY += gp.tileSize;
        g2.drawString("ESC", textX, textY); textY += gp.tileSize;

        //BACK
        textX = frameX + gp.tileSize - 31;
        textY = frameY + gp.tileSize * 9;
        g2.drawString("Back", textX, textY);
        if(commandNum == 0) {
            g2.drawString(">", textX-17, textY);
             if(gp.keyH.enterPressed){
                 subState = 0;
                 commandNum = 1;
            }
        }
    }

    /**
     * Renders the END GAME section of the options window, the game resets.
     *
     * @param frameX the x-coordinate of the options window frame for writing text
     * @param frameY the y-coordinate of the options window frame for writing text
     */
    public void options_endGameConfirmation(int frameX, int frameY){
        g2.setColor(Color.white);
        g2.setFont(g2.getFont().deriveFont(28F));
        int textX = frameX + gp.tileSize - 30;
        int textY = frameY + gp.tileSize;

        currentDialogue = "Quit the game and\nreturn to the title\nscreen?";

        for(String line: currentDialogue.split("\n")){
            g2.drawString(line, textX, textY);
            textY += 40;
        }
        //YES
        String text = "Yes";
        textX = getXforCenteredText(text);
        textY += gp.tileSize*3;
        g2.drawString(text, textX, textY);
        if(commandNum == 0) {
            g2.drawString(">", textX-25, textY);
            if(gp.keyH.enterPressed == true) {
                subState = 0;
                gp.gameState = gp.titleState;
                gp.resetGame(true);
            }
        }
        //NO
        text = "No";
        textX = getXforCenteredText(text);
        textY += gp.tileSize;
        g2.drawString(text, textX, textY);
        if(commandNum == 1) {
            g2.drawString(">", textX-25, textY);
            if(gp.keyH.enterPressed == true) {
                subState = 0;
                commandNum = 1;
            }
        }
    }

    public int getItemIndexOnSlot() {
        int itemIndex = slotCol + (slotRow * 5);
        return itemIndex;
    }

    /**
     * Draws a subwindow (rectangle) for the dialogue screen.
     *
     * @param x      the x-coordinate of the subwindow
     * @param y      the y-coordinate of the subwindow
     * @param width  the width of the subwindow
     * @param height the height of the subwindow
     */
    public void drawSubWindow(int x, int y, int width, int height) { //window(rectangle) for dialogue screen

        Color c = new Color(0, 0, 0, 150);
        g2.setColor(c);
        g2.fillRoundRect(x, y, width, height, 35, 35); //"roundness" of rectangle

    }

    /**
     * Calculates the x-coordinate for displaying text centered on the screen.
     *
     * @param text the text to be centered
     * @return the x-coordinate for centered text
     */
    public int getXforCenteredText(String text) { //for comfort not to count x coordinates each time we want to show text
        //at the center of the screen

        int length = (int)g2.getFontMetrics().getStringBounds(text, g2).getWidth();
        int x = gp.screenWidth/2 - length/2;
        return x;
    }

    /**
     * Calculates the x-coordinate for displaying text aligned to the right.
     *
     * @param text   the text to be aligned to the right
     * @param tailX  the x-coordinate of the tail point (right end)
     * @return the x-coordinate for text aligned to the right
     */
    public int getXforAlignToRightText(String text, int tailX) {
        //aligned to the right

        int length = (int)g2.getFontMetrics().getStringBounds(text, g2).getWidth();
        int x = tailX - length; //we start writing the text on this x coordinate
        return x;
    }
}
