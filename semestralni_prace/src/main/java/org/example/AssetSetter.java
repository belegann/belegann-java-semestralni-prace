package org.example;


import org.example.entity.NPC_Pepe;
import org.example.object.*;
import org.example.monster.*;
import org.example.tiles_interactive.*;


public class AssetSetter {

    GamePanel gp;

    public AssetSetter(GamePanel gp) {
        this.gp = gp;
    }

    public void setObject() {

        int i = 0;
        gp.obj[i] = new OBJ_Coin(gp); //position of the coin
        gp.obj[i].worldX = 4 * gp.tileSize;
        gp.obj[i].worldY = 8 * gp.tileSize;

        i++;
        gp.obj[i] = new OBJ_Axe(gp); //position of the first axe
        gp.obj[i].worldX = 8 * gp.tileSize;
        gp.obj[i].worldY = 32 * gp.tileSize;

        i++;
        gp.obj[i] = new OBJ_Shield_forest(gp); //position of forest shield
        gp.obj[i].worldX = 13 * gp.tileSize;
        gp.obj[i].worldY = 43 * gp.tileSize;

        i++;
        gp.obj[i] = new OBJ_Potion_Gold(gp); //position of the gold potion
        gp.obj[i].worldX = 5 * gp.tileSize;
        gp.obj[i].worldY = 4 * gp.tileSize;

        i++;
        gp.obj[i] = new OBJ_chest(gp); //position of the chest
        gp.obj[i].worldX = 3 * gp.tileSize;
        gp.obj[i].worldY = 4 * gp.tileSize;

    }

    public void setNPC() {

        gp.npc[0] = new NPC_Pepe(gp); //instance
        gp.npc[0].worldX = gp.tileSize * 23; //position x
        gp.npc[0].worldY = gp.tileSize * 5; //position y

    }

    public void setMonster() {

        int i = 0;

        gp.monster[i] = new MON_GoldAmogus(gp);
        gp.monster[i].worldX = gp.tileSize * 3;
        gp.monster[i].worldY = gp.tileSize * 20;

        i++;
        gp.monster[i] = new MON_GoldAmogus(gp);
        gp.monster[i].worldX = gp.tileSize * 6;
        gp.monster[i].worldY = gp.tileSize * 20;

        i++;
        gp.monster[i] = new MON_GoldAmogus(gp);
        gp.monster[i].worldX = gp.tileSize * 5;
        gp.monster[i].worldY = gp.tileSize * 21;


        i++;
        gp.monster[i] = new MON_ForestAmogus(gp);
        gp.monster[i].worldX = gp.tileSize * 9;
        gp.monster[i].worldY = gp.tileSize * 30;
        i++;
        gp.monster[i] = new MON_ForestAmogus(gp);
        gp.monster[i].worldX = gp.tileSize * 10;
        gp.monster[i].worldY = gp.tileSize * 31;

        i++;
        gp.monster[i] = new MON_ForestAmogus(gp);
        gp.monster[i].worldX = gp.tileSize * 11;
        gp.monster[i].worldY = gp.tileSize * 44;
        i++;
        gp.monster[i] = new MON_ForestAmogus(gp);
        gp.monster[i].worldX = gp.tileSize * 10;
        gp.monster[i].worldY = gp.tileSize * 43;

    }

    public void setInteractiveTile(){
        int i = 0;

        gp.iTile[i] = new IT_DryTree(gp, 22, 25);
        i++;
        gp.iTile[i] = new IT_DryTree(gp, 22, 26);
        i++;
        gp.iTile[i] = new IT_DryTree(gp, 12, 43);
        i++;
        gp.iTile[i] = new IT_DryTree(gp, 12, 42);
        i++;
        gp.iTile[i] = new IT_DryTree(gp, 12, 44);
    }
}
