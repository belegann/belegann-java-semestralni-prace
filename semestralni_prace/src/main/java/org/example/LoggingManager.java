package org.example;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LoggingManager {
    private static final Logger logger = Logger.getLogger("MyLogger");

    static {
        // Set log level and configure logger
        logger.setLevel(Level.SEVERE);

        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(Level.SEVERE);
        consoleHandler.setFormatter(new SimpleFormatter());

        logger.addHandler(consoleHandler);
    }

    public static Logger getLogger() {
        return logger;
    }
}
