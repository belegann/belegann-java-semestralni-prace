package org.example;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {

        JFrame window = new JFrame(); //creating window
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //closing when user clicks X button
        window.setResizable(false); //we can not change the size of window
        window.setTitle("Semestral project belegann"); //set the name


        GamePanel gamePanel = new GamePanel();
        window.add(gamePanel);

        window.pack(); //? causes this window to be sized to fit the size of its subcomponents (=GamePanel)
        window.setLocationRelativeTo(null);  //the window will be at the center of the screen
        window.setVisible(true); //we can see the screen

        gamePanel.startGameThread(); //starting a game
        gamePanel.setupGame(); //for setting objects
    }
}